#include"Cube.h"
#include "pg2_vertexbuffer.h"
#include "pg2_indexbuffer.h"

VERTEX cubeStructure[] = {
	{ -0.5f, 0.5f, -0.5f, D3DCOLOR_XRGB(255, 0, 0) }, // 0 
	{ 0.5f, 0.5f, -0.5f, D3DCOLOR_XRGB(0, 255, 0) }, // 1 
	{ 0.5f, 0.5f, 0.5f, D3DCOLOR_XRGB(40, 0, 120) }, // 2 
	{ -0.5f, 0.5f, 0.5f, D3DCOLOR_XRGB(255, 0, 0) }, // 3

	{ -0.5f, -0.5f, 0.5f, D3DCOLOR_XRGB(0, 255, 0) }, // 4
	{ 0.5f, -0.5f, 0.5f, D3DCOLOR_XRGB(40, 0, 120) }, // 5
	{ 0.5f, -0.5f, -0.5f, D3DCOLOR_XRGB(255, 0, 0) }, // 6
	{ -0.5f, -0.5f, -0.5f, D3DCOLOR_XRGB(0, 255, 0) } // 7
};

USHORT cubeIndexes[] = { 0, 1, 2, 0, 2, 3,
4, 5, 6, 4, 6, 7,
3, 2, 5, 3, 5, 4,
2, 1, 6, 2, 6, 5,
1, 7, 6, 1, 0, 7,
0, 3, 4, 0, 4, 7 };

Cube::Cube(bool isTextured){
	firstDraw = false;
	tex = NULL;
	textured = isTextured;
}

Cube::~Cube(){
	if (tex)
		delete tex;
}

void Cube::draw(DxClass * renderer){
	if (!firstDraw){
		if (textured)
			vb = renderer->CreateVertexBuffer3D(sizeof(UVVERTEX), UVFVF);
		else
			vb = renderer->CreateVertexBuffer3D(sizeof(VERTEX), CUSTOMFVF);
		ib = renderer->CreateIndexBuffer();
		vb->setVertexData((void*)cubeStructure, 8);
		ib->setIndexData(cubeIndexes, 36);
		type = D3DPT_TRIANGLELIST;
		vertexCount = 8;
		indexCount = 36;
		firstDraw = true;
	}

	if (textured)
		renderer->setTexture(tex);

	vb->bind();
	ib->bind();
	renderer->setMatrix(World, transformMatrix);
	renderer->draw3d(type);
}

void Cube::setTexture(Texture texture){
	tex = texture;
}

bool Cube::canBeDrawn(Camera * mainCamera){
	return true;
}
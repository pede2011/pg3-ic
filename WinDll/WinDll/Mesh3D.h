#ifndef MESH3D_H
#define MESH3D_H

#include"Entity3D.h"

class VertexBuffer3D;
class IndexBuffer;
class DxClass;
class D3DXPLANE;
class D3DXVECTOR3;

class GN Mesh3D : public Entity3D{
public:
	Mesh3D();
	~Mesh3D();
	bool canBeDrawn(Camera * mainCamera, std::list<D3DXPLANE*> bspPlanes, bool drawPlanes = false);
	void setTexture(Texture texture);
	void draw(DxClass * renderer);
	void scale(float x, float y, float z);
	void setTexturePath(std::string path);
	D3DXPLANE * getBspPlane();

private:
	void calculateSphereRadius(VERTEX * vertices, size_t count);
	void calculateSphereRadius(UVVERTEX * vertices, size_t count);
	Texture tex;
	bool firstDraw;
	bool textured;
	float originalSphereRadius;
	float boundingSphereRadius;
	std::string texturePath;

	VertexBuffer3D* vb;
	IndexBuffer* ib;

	D3DPRIMITIVETYPE type;
	size_t vertexCount;
	size_t indexCount;

	UVVERTEX * meshStructureUV;
	VERTEX * meshStructure;
	USHORT * meshIndexes;
	D3DXVECTOR3 * boundingSphereCentre;
	D3DXVECTOR3 * boundingSphereWorld;

	
protected:
	void setStructure(UVVERTEX * structure, USHORT * indexes, size_t vtxCount, size_t indCount);
	void setStructure(VERTEX * structure, USHORT * indexes, size_t vtxCount, size_t indCount);
	void applyTransformation();
	void applyTransformation(D3DXMATRIX * parentMatrix);

	friend class GameObject;
	friend class Importer;
	D3DXVECTOR3 * planeP1;
	D3DXVECTOR3 * planeP2;
	D3DXVECTOR3 * planeP3;
};

#endif
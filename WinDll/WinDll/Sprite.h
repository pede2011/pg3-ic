#ifndef SPRITE_H
#define SPRITE_H

#include"Entity2D.h"
#include"Animation.h"

class GN Sprite : public Entity2D{
public:
	Sprite();
	~Sprite();
	void setTexture(Texture texture);
	void setCoords(double u1, double v1,
		double u2, double v2,
		double u3, double v3,
		double u4, double v4);
	void draw(DxClass * renderer);
	void setAnimation(Animation* animation);
	void update(Timer * timer);
	void layer(int layer);
	bool canBeDrawn(Camera * mainCamera, std::list<D3DXPLANE*> bspPlanes, bool drawPlanes = false);
private:
	Texture tex;
	UVVERTEX * vertices;
	Animation * anim;
	int prevFrame;
};

#endif
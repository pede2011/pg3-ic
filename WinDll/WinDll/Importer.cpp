#include"Importer.h"
#include"Mesh3D.h"

#include"Importer.hpp"
#include"scene.h"
#include"postprocess.h"
#include"material.h"

#include<limits>
#include<cstddef>
#include<d3dx9.h>

Importer::Importer(){}

void Importer::loadMesh(Mesh3D * targetMesh, std::string filename){
	Assimp::Importer importer;
	const aiScene * scene = importer.ReadFile(filename, aiPrimitiveType_LINE | aiPrimitiveType_POINT | aiProcess_Triangulate | aiProcess_SortByPType | aiProcess_MakeLeftHanded);
	//const aiScene * scene = importer.ReadFile(filename, aiProcess_CalcTangentSpace | aiProcess_Triangulate | aiProcess_JoinIdenticalVertices | aiProcess_SortByPType);

	if (!scene) return;
	if (!scene->mMeshes[0])
		return;
	size_t indCount;
	size_t vtxCount;
	bool isTextured = false;

	VERTEX * structure = NULL;
	UVVERTEX * structureUV = NULL;
	USHORT * indices;
	aiMesh * mesh = scene->mMeshes[0];

	//NAME
	//targetMesh->setName(mesh->mName.C_Str());
	//END NAME

	//FACES
	if (!mesh->HasFaces())
		return;
	targetMesh->faceQty = mesh->mNumFaces;
	aiFace * faces = mesh->mFaces;
	indCount = mesh->mNumFaces * 3;
	indices = new USHORT[indCount];
	for (DWORD i = 0; i < mesh->mNumFaces; i++){
		if (faces[i].mNumIndices != 3){
			int d = faces[i].mNumIndices;

			delete indices;
			return;
		}
		for (int j = 0; j < 3; j++){
			indices[i * 3 + j] = faces[i].mIndices[j];
		}
	}//END FACES

	 //BSP PLANE
	D3DXVECTOR3 * p1 = new D3DXVECTOR3();
	D3DXVECTOR3 * p2 = new D3DXVECTOR3();
	D3DXVECTOR3 * p3 = new D3DXVECTOR3();

	p1->x = mesh->mVertices[0].x;
	p1->y = mesh->mVertices[0].y;
	p1->z = mesh->mVertices[0].z;

	p2->x = mesh->mVertices[1].x;
	p2->y = mesh->mVertices[1].y;
	p2->z = mesh->mVertices[1].z;

	p3->x = mesh->mVertices[2].x;
	p3->y = mesh->mVertices[2].y;
	p3->z = mesh->mVertices[2].z;
	//END BSP PLANE

	//POSITIONS
	if (!mesh->HasPositions())
		return;
	vtxCount = mesh->mNumVertices;
	if (mesh->HasTextureCoords(0))
		isTextured = true;
	else
		isTextured = false;
	
	if (isTextured)
		structureUV = new UVVERTEX[vtxCount];
	else
		structure = new VERTEX[vtxCount];

	for (int i = 0; i < vtxCount; i++){
		if (isTextured){
			structureUV[i].x = mesh->mVertices[i].x;
			structureUV[i].y = mesh->mVertices[i].y;
			structureUV[i].z = mesh->mVertices[i].z;
		}
		else{
			structure[i].x = mesh->mVertices[i].x;
			structure[i].y = mesh->mVertices[i].y;
			structure[i].z = mesh->mVertices[i].z;
		}
	}//END POSITIONS

	//TEXTURES
	if (isTextured){
		for (int i = 0; i < vtxCount; i++){
			structureUV[i].u = mesh->mTextureCoords[0][i].x;
			structureUV[i].v = mesh->mTextureCoords[0][i].y;
		}
		aiString path;
		scene->mMaterials[0]->GetTexture(aiTextureType_DIFFUSE, 0, &path);
		targetMesh->setTexturePath(path.data);
	}
	else{
		for (int i = 0; i < vtxCount; i++){
			structure[i].color = D3DCOLOR_XRGB(200, 200, 200);
		}
	}//END TEXTURES

	if (isTextured)
		targetMesh->setStructure(structureUV, indices, vtxCount, indCount);
	else
		targetMesh->setStructure(structure, indices, vtxCount, indCount);
}

void Importer::loadMesh(Mesh3D * targetMesh, const aiScene * scene, int index){
	if (!scene) return;
	if (!scene->mMeshes[index])
		return;
	size_t indCount;
	size_t vtxCount;
	bool isTextured = false;

	VERTEX * structure = NULL;
	UVVERTEX * structureUV = NULL;
	USHORT * indices;
	aiMesh * mesh = scene->mMeshes[index];

	//FACES
	if (!mesh->HasFaces())
		return;
	targetMesh->faceQty = mesh->mNumFaces;
	aiFace * faces = mesh->mFaces;
	indCount = mesh->mNumFaces * 3;
	indices = new USHORT[indCount];
	for (DWORD i = 0; i < mesh->mNumFaces; i++){
		if (faces[i].mNumIndices != 3){
			int d = faces[i].mNumIndices;

			delete indices;
			return;
		}
		for (int j = 0; j < 3; j++){
			indices[i * 3 + j] = faces[i].mIndices[j];
		}
	}//END FACES

	//POSITIONS
	if (!mesh->HasPositions())
		return;
	vtxCount = mesh->mNumVertices;
	if (mesh->HasTextureCoords(0))
		isTextured = true;
	else
		isTextured = false;

	if (isTextured)
		structureUV = new UVVERTEX[vtxCount];
	else
		structure = new VERTEX[vtxCount];

	for (int i = 0; i < vtxCount; i++){
		if (isTextured){
			structureUV[i].x = mesh->mVertices[i].x;
			structureUV[i].y = mesh->mVertices[i].y;
			structureUV[i].z = mesh->mVertices[i].z;
		}
		else{
			structure[i].x = mesh->mVertices[i].x;
			structure[i].y = mesh->mVertices[i].y;
			structure[i].z = mesh->mVertices[i].z;
		}
	}//END POSITIONS

	//BSP PLANE
	D3DXVECTOR3 * p1 = new D3DXVECTOR3();
	D3DXVECTOR3 * p2 = new D3DXVECTOR3();
	D3DXVECTOR3 * p3 = new D3DXVECTOR3();

	p1->x = mesh->mVertices[0].x;
	p1->y = mesh->mVertices[0].y;
	p1->z = mesh->mVertices[0].z;

	p2->x = mesh->mVertices[1].x;
	p2->y = mesh->mVertices[1].y;
	p2->z = mesh->mVertices[1].z;

	p3->x = mesh->mVertices[2].x;
	p3->y = mesh->mVertices[2].y;
	p3->z = mesh->mVertices[2].z;

	targetMesh->planeP1 = p1;
	targetMesh->planeP2 = p2;
	targetMesh->planeP3 = p3;
	//END BSP PLANE

	//TEXTURES
	if (isTextured){
		for (int i = 0; i < vtxCount; i++){
			structureUV[i].u = mesh->mTextureCoords[0][i].x;
			structureUV[i].v = 1-mesh->mTextureCoords[0][i].y;
		}
		aiString path;
		scene->mMaterials[mesh->mMaterialIndex]->GetTexture(aiTextureType_DIFFUSE, 0, &path);
		targetMesh->setTexturePath(path.data);
	}
	else{
		for (int i = 0; i < vtxCount; i++){
			structure[i].color = D3DCOLOR_XRGB(200, 200, 200);
		}
	}//END TEXTURES

	if (isTextured)
		targetMesh->setStructure(structureUV, indices, vtxCount, indCount);
	else
		targetMesh->setStructure(structure, indices, vtxCount, indCount);
}
#ifndef ENGINE_H
#define ENGINE_H

#ifndef GN
#define GN __declspec(dllexport)
#else
#define GN __declspec(dllimport)
#endif

#include"Ventana.h"
#include"Game.h"
#include"pg1_directinput.h"
#include"Timer.h"
#include <sstream>
#include <string>

#include <io.h>         //_open_osfhandle
#include <fcntl.h>      //_O_TEXT

class GN Engine{
public:
	Engine(HINSTANCE hInstance, unsigned int w = 640, unsigned int h = 480);
	~Engine();
	bool init(Game * g);
	void run();
	void RedirectIOToConsole();
	static const WORD MAX_CONSOLE_LINES = 1000;
private:
	DirectInput * dInput;
	Game * game;
	Ventana * win;
	DxClass * renderer;
	HINSTANCE winhInstance;
	unsigned int winW;
	unsigned int winH;
	Timer * timer;
};
#endif
#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H

#include"Mesh3D.h"
//-
#include"Importer.h"
#include"Importer.hpp"
#include"scene.h"
#include"postprocess.h"
//-
#include<vector>
#include<list>

class GN GameObject : public Entity3D{
public:
	GameObject();
	~GameObject();
	void translate(float x, float y, float z);
	void setPosition(float x, float y, float z);
	void rotate(float x, float y, float z);
	void setRotation(float x, float y, float z);
	void scale(float x, float y, float z);
	void draw(DxClass * renderer);
	void loadModel(std::string filename);
	void loadModelWithHierarchy(std::string filename);
	void loadModelWithHierarchy(std::string filename, std::string bspplanename);
	void add(GameObject * object);
	void setActive(bool state);
	bool canBeDrawn(Camera * mainCamera, std::list<D3DXPLANE*> bspPlanes, bool drawPlanes = false);
	GameObject * getChild(int index);
	Mesh3D * getMesh(int index);
private:
	std::vector<GameObject*> children;
	std::vector<Mesh3D*> meshChildren;
	void applyTransformToChildren();
	void loadHierarchy(aiNode * node, const aiScene * scene);
	void parseMatrix(D3DXMATRIX * m1, aiMatrix4x4 * m2);
	D3DXMATRIX * parentMatrix;
	Camera * mainCameraReference;
	std::list<D3DXPLANE*> bspPlanesReference;
	bool drawPlanesRef;
	std::string BspPlaneName;
protected:
	friend class Game;
	std::list<D3DXPLANE*> generateBsp();
};

#endif
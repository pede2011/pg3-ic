#ifndef CUBE_H
#define CUBE_H

#include"Entity3D.h"

class VertexBuffer3D;
class IndexBuffer;
class DxClass;

class GN Cube : public Entity3D{
public:
	Cube(bool isTextured=false);
	~Cube();
	void setTexture(Texture texture);
	void draw(DxClass * renderer);
	bool canBeDrawn(Camera * mainCamera);
private:
	Texture tex;
	bool firstDraw;
	bool textured;

	VertexBuffer3D* vb;
	IndexBuffer* ib;

	D3DPRIMITIVETYPE type;
	size_t vertexCount;
	size_t indexCount;
};

#endif
#include"Sprite.h"
#include<d3dx9.h>

#ifdef max
#undef max
#endif

Sprite::Sprite(){
	tex = NULL;
	anim = NULL;
	prevFrame = std::numeric_limits<int>::max();
	vertices = new UVVERTEX[4];
	vertices[0].x = -0.5f;	vertices[1].x = 0.5f;	vertices[2].x = -0.5f;	vertices[3].x = 0.5f;
	vertices[0].y = 0.5f;	vertices[1].y = 0.5f;	vertices[2].y = -0.5f;	vertices[3].y = -0.5f;
	vertices[0].z = 0;		vertices[1].z = 0;		vertices[2].z = 0;		vertices[3].z = 0;
	vertices[0].u = 0;		vertices[1].u = 1;		vertices[2].u = 0;		vertices[3].u = 1;
	vertices[0].v = 0;		vertices[1].v = 0;		vertices[2].z = 1;		vertices[3].v = 1;
}

Sprite::~Sprite(){
	delete[] vertices;
	delete anim;
	anim = NULL;
	vertices = NULL;
}

void Sprite::layer(int layer){
	vertices[0].z = layer;
	vertices[1].z = layer;
	vertices[2].z = layer;
	vertices[3].z = layer;
}

void Sprite::setCoords(double u1, double v1,
	double u2, double v2,
	double u3, double v3,
	double u4, double v4){
	vertices[0].u = u1;	vertices[1].u = u2;	vertices[2].u = u3;	vertices[3].u = u4;
	vertices[0].v = v1;	vertices[1].v = v2;	vertices[2].v = v3;	vertices[3].v = v4;
}

void Sprite::setTexture(Texture texture){
	tex = texture;
}

void Sprite::draw(DxClass * renderer){
	renderer->setTexture(tex);
	renderer->setMatrix(World, transformMatrix);
	renderer->draw(vertices);
}

void Sprite::setAnimation(Animation * animation){
	anim = animation;
}

void Sprite::update(Timer * timer){
	if (!anim)
		return;
	anim->update(timer);
	int currentFrame = anim->currentFrame();
	if (currentFrame != prevFrame){
		Frame & frame = anim->frames()[currentFrame];
		setCoords(frame.u1, frame.v1, frame.u2, frame.v2, frame.u3, frame.v3, frame.u4, frame.v4);
	}
}

bool Sprite::canBeDrawn(Camera * mainCamera, std::list<D3DXPLANE*> bspPlanes, bool drawPlanes){
	return true;
}
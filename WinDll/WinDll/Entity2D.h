#ifndef ENTITY2D_H
#define ENTITY2D_H

#include"DxClass.h"
#include<list>

class D3DXPLANE;

enum GN Axis{
	Vertical,
	Horizontal
};

class GN Entity2D{
public:
	Entity2D();
	~Entity2D();
	virtual void translate(float x, float y);
	virtual void setPosition(float x, float y);
	virtual void rotate(float z);
	virtual void scale(float x, float y);
	virtual void draw(DxClass * renderer) = 0;
	virtual void layer(int layer) = 0;
	virtual void setActive(bool state);
	virtual bool canBeDrawn(Camera * mainCamera, std::list<D3DXPLANE*> bspPlanes, bool drawPlanes=false)=0;
	float getX() const;
	float getY() const;
	float getRotation() const;
	float getScaleX() const;
	float getScaleY() const;
	bool isActive();
	void setRigidBody(bool enableRigidBody=true, float mass=1.0f);
	float getMass();
	bool isRigidBody();
	bool isBspPlane();
	bool isDebugBspPlane();
	std::string getName();
	void setName(std::string newName);

	int drawableMeshes;
	int faceQty;
	int drawableFaces;
	int meshQuantity;

protected:
	D3DXMATRIX * transformMatrix;
	D3DXPLANE * bspPlane;
	bool active;
	virtual void applyTransformation();
	float posx, posy, rotation, scalex, scaley, _mass;
	bool isRigidBodyEnabled;
	bool isBsp;
	bool isDebugBsp;
	std::string name;
};

#endif
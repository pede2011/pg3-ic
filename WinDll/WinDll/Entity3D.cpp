#include"Entity3D.h"
#include<d3dx9.h>

Entity3D::Entity3D(){
	active = true;
	isRigidBodyEnabled = false;
	_mass = 1.0f;
	posx = 0;
	posy = 0;
	posz = 0;
	rotx = 0;
	roty = 0;
	rotz = 0;
	scalex = 1;
	scaley = 1;
	scalez = 1;
	layerPos = 0;
	transformMatrix = new D3DXMATRIX();
	m_OriginalTransform = new D3DXMATRIX();
	D3DXMatrixIdentity(m_OriginalTransform);
	applyTransformation();

	drawableMeshes = 0;
	drawableFaces = 0;
}

Entity3D::~Entity3D(){
	delete transformMatrix;
	transformMatrix = NULL;
}

void Entity3D::translate(float x, float y, float z){
	posx += x;
	posy += y;
	posz += z;
	applyTransformation();
}

void Entity3D::setPosition(float x, float y, float z){
	posx = x;
	posy = y;
	posz = z;
	applyTransformation();
}

void Entity3D::rotate(float x, float y, float z){
	rotx += x;
	roty += y;
	rotz += z;
	applyTransformation();
}

void Entity3D::setRotation(float x, float y, float z){
	rotx = x;
	roty = y;
	rotz = z;
	applyTransformation();
}

void Entity3D::scale(float x, float y, float z){
	scalex = x;
	scaley = y;
	scalez = z;
	applyTransformation();
}

void Entity3D::applyTransformation(){
	D3DXMATRIX trans;
	D3DXMATRIX mRotZ;
	D3DXMATRIX mRotX;
	D3DXMATRIX mRotY;
	D3DXMATRIX rot;
	D3DXMATRIX scl;
	D3DXMatrixTranslation(&trans, posx, posy, posz);

	D3DXMatrixRotationZ(&mRotZ, D3DXToRadian(rotz));
	D3DXMatrixRotationY(&mRotY, D3DXToRadian(roty));
	D3DXMatrixRotationX(&mRotX, D3DXToRadian(rotx));

	D3DXMatrixIdentity(&rot);
	D3DXMatrixMultiply(&rot, &mRotZ, &mRotY);
	D3DXMatrixMultiply(&rot, &mRotX, &rot);

	D3DXMatrixScaling(&scl, scalex, scaley, scalez);

	D3DXMatrixIdentity(transformMatrix);
	D3DXMatrixMultiply(transformMatrix, &trans, transformMatrix);
	D3DXMatrixMultiply(transformMatrix, &rot, transformMatrix);
	D3DXMatrixMultiply(transformMatrix, &scl, transformMatrix);

	D3DXMatrixMultiply(transformMatrix, m_OriginalTransform, transformMatrix);

	//transformMatrix = &(rot*trans*scl);
	//transformMatrix = &(trans*rot*scl);
}

void Entity3D::applyTransformation(D3DXMATRIX * parentMatrix){
	D3DXMATRIX trans;
	D3DXMATRIX mRotZ;
	D3DXMATRIX mRotX;
	D3DXMATRIX mRotY;
	D3DXMATRIX rot;
	D3DXMATRIX scl;
	D3DXMatrixTranslation(&trans, posx, posy, posz);

	D3DXMatrixRotationZ(&mRotZ, D3DXToRadian(rotz));
	D3DXMatrixRotationY(&mRotY, D3DXToRadian(roty));
	D3DXMatrixRotationX(&mRotX, D3DXToRadian(rotx));

	D3DXMatrixIdentity(&rot);
	D3DXMatrixMultiply(&rot, &mRotZ, &mRotY);
	D3DXMatrixMultiply(&rot, &mRotX, &rot);

	D3DXMatrixScaling(&scl, scalex, scaley, scalez);

	D3DXMatrixIdentity(transformMatrix);

	D3DXMatrixMultiply(transformMatrix, m_OriginalTransform, transformMatrix);

	D3DXMatrixMultiply(transformMatrix, &trans, transformMatrix);
	D3DXMatrixMultiply(transformMatrix, &rot, transformMatrix);
	D3DXMatrixMultiply(transformMatrix, &scl, transformMatrix);
	
	//D3DXMatrixMultiply(transformMatrix, m_OriginalTransform, transformMatrix);
	D3DXMatrixMultiply(transformMatrix, transformMatrix, parentMatrix);


	//transformMatrix = &(rot*trans*scl);
	//transformMatrix = &(trans*rot*scl);
}

float Entity3D::getZ() const{
	return posz;
}

float Entity3D::getRotationX() const{
	return rotx;
}

float Entity3D::getRotationY() const{
	return roty;
}

float Entity3D::getRotationZ() const{
	return rotz;
}

float Entity3D::getScaleZ() const{
	return scalez;
}

void Entity3D::layer(int layer){
	layerPos = layer;
}
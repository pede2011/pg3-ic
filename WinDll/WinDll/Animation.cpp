#include"Animation.h"

Animation::Animation(){
	_lenght = 1.0f;
	currentTime = 0;
	_currentFrame = 0;
}

int Animation::currentFrame(){
	return _currentFrame;
}

std::vector<Frame> & Animation::frames(){
	return _frames;
}

void Animation::setLenght(float lenght){
	_lenght = lenght;
}

void Animation::addFrame(float texWidth, float texHeight, float x, float y, float width, float height){
	Frame frame;
	frame.u1 = (x / texWidth);
	frame.v1 = (y / texHeight);
	frame.u2 = ((x + width) / texWidth);
	frame.v2 = (y / texHeight);
	frame.u3 = (x / texWidth);
	frame.v3 = ((y + height) / texHeight);
	frame.u4 = ((x + width) / texWidth);
	frame.v4 = ((y + height) / texHeight);
	_frames.push_back(frame);
}

void Animation::addFrame(Frame frame){
	_frames.push_back(frame);
}

void Animation::update(Timer * timer){
	currentTime += timer->deltaTime();
	while (currentTime > _lenght){
		currentTime -= _lenght;
	}
	_currentFrame = static_cast<int>((currentTime / _lenght) * _frames.size());
}
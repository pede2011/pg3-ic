#include"Ventana.h"

LRESULT CALLBACK WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam){
	switch (msg){
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hwnd, msg, wParam, lParam);
	}
	return 0;
}

bool Ventana::CrearVentana(HINSTANCE hInstance, unsigned int w, unsigned int h){

	WNDCLASSEX wc;
	const char winName[] = "Average Window";
	MSG msg;

	wc.cbSize = sizeof(WNDCLASSEX);
	wc.style = 0;
	wc.lpfnWndProc = WndProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = hInstance;
	wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wc.lpszMenuName = NULL;
	wc.lpszClassName = winName;
	wc.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	if (!RegisterClassEx(&wc)){
		MessageBox(NULL, "Fallo el registro de la ventana", "Error.", MB_ICONEXCLAMATION | MB_OK);
		return false;
	}

	HWND vent;
	Ventana ventanita;
	vent = CreateWindow(winName, "Game", WS_OVERLAPPEDWINDOW | WS_VISIBLE, 0, 0, w, h, NULL, NULL, hInstance, NULL);
	win = vent;

	if (vent == NULL){
		MessageBox(NULL, "Fallo la creacion de ventana", "Error.", MB_ICONEXCLAMATION | MB_OK);
		return false;
	}

	ShowWindow(vent, 1);
	UpdateWindow(vent);

	return true;
}

HWND Ventana::hwnd() const{
	return win;
}
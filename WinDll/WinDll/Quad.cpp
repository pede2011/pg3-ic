#include"Quad.h"
#include<d3dx9.h>

Quad::Quad(){
	shape = (new VERTEX[4]);
	shape[0].x = -0.5f; shape[0].y = -0.5f; shape[0].z = 0.0f; shape[0].color = D3DCOLOR_ARGB(255, 0, 0, 255);
	shape[1].x = -0.5f; shape[1].y = 0.5f; shape[1].z = 0.0f; shape[1].color = D3DCOLOR_ARGB(128, 0, 255, 0);
	shape[2].x = 0.5f; shape[2].y = -0.5f; shape[2].z = 0.0f; shape[2].color = D3DCOLOR_ARGB(60, 255, 0, 0);
	shape[3].x = 0.5f; shape[3].y = 0.5f; shape[3].z = 0.0f; shape[3].color = D3DCOLOR_ARGB(0, 0, 255, 255);
}

Quad::~Quad(){
	delete[] shape;
	shape = NULL;
}

void Quad::draw(DxClass* renderer){
	Texture nullTexture = NULL;
	renderer->setTexture(nullTexture);
	renderer->setMatrix(World, transformMatrix);
	renderer->draw(shape, D3DPT_TRIANGLESTRIP, 4);
}

void Quad::layer(int layer){
	shape[0].z = layer;
	shape[1].z = layer;
	shape[2].z = layer;
	shape[3].z = layer;
}

bool Quad::canBeDrawn(Camera * mainCamera){
	return true;
}
#ifndef GAME_H
#define GAME_H

#include"DxClass.h"
#include"pg1_directinput.h"
#include"Timer.h"
#include"Tilemap.h"
#include"Camera.h"
#include<list>
class TMXLoader;
class D3DXPLANE;
class GameObject;

class GN Game{
private:
	bool finished;
	std::list<Entity2D*> renderList;
	std::list<Sprite*> tileList;
	DirectInput * dinput;
	Timer * gtimer;
	bool stob(std::string s);
	std::map<int, Frame> getFrameList(TMXLoader * map, std::string const mapName, std::string const tilesetName);
	Camera * mainCamera;
	bool drawBspPlanes;
	std::list<D3DXPLANE*> bspPlanes;
protected:
	void add(Entity2D * entity);
	void add(Tilemap * map);
	bool collision(Entity2D * a, Entity2D * b, bool isTrigger = false);
	Texture loadTexture(std::string const filename, int colorKey=0xFFFFFFFF);
	Tilemap * loadTilemap(std::string const mapFileName, std::string const mapName, std::string const spriteSheetFileName, std::string const layerName, std::string const tilesetName);
	DirectInput * input();
	Timer * timer();
	DxClass * rend;
	void setMainCamera(Camera * camera, float FieldOfView=90, float NearClippingPlane=0.1f, float FarClippingPlane=3000);
	void switchBspPlaneDraw();
	void generateBspPlanes(GameObject * scene);
	void clearBspList();
public:
	Game();
	~Game();
	virtual void Start() = 0;
	virtual void Update() = 0;
	virtual void Shutdown() = 0;
	void finishGame();
	bool isFinished();
	void setRenderer(DxClass * renderer);
	void setInput(DirectInput * dInput);
	void setTimer(Timer * timer);
	std::list<Entity2D*>& getRenderList();
	std::list<Sprite*>& getTiles();
	Camera * getMainCamera();
	bool bspDrawState();
	std::list<D3DXPLANE*> getBspPlanes();
};
#endif
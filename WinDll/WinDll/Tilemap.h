#ifndef TILEMAP_H
#define TILEMAP_H

#include"Sprite.h"
#include<map>
#include<list>

class GN Tilemap : public Entity2D{
private:
	std::map<Sprite*, std::string> tiles;
	void collide(Entity2D * a, Entity2D * b);
public:
	void addTile(Sprite * tile, std::string canCollide);
	void draw(DxClass * renderer);
	void translate(float x, float y);
	void setPosition(float x, float y);
	void rotate(float z);
	void scale(float x, float y);
	void setActive(bool state);
	void layer(int layer);
	void checkCollision(Entity2D * object);
	std::map<Sprite*, std::string> getTiles();
	bool canBeDrawn(Camera * mainCamera, std::list<D3DXPLANE*> bspPlanes, bool drawPlanes = false);
};

#endif
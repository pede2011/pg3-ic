#include"GameObject.h"
/*
#include"Importer.h"
#include"Importer.hpp"
#include"scene.h"
#include"postprocess.h"
*/
#include<d3dx9.h>

GameObject::GameObject(){
	parentMatrix = NULL;
	faceQty = 0;
	meshQuantity = 0;
}

GameObject::~GameObject(){
	for (std::vector<Mesh3D*>::iterator i = meshChildren.begin(); i != meshChildren.end(); i++){
		(*i)->~Mesh3D();
	}
	for (std::vector<GameObject*>::iterator i = children.begin(); i != children.end(); i++){
		(*i)->~GameObject();
	}
}

void GameObject::translate(float x, float y, float z){
	posx += x;
	posy += y;
	posz += z;
	if (parentMatrix != NULL)
		applyTransformation(parentMatrix);
	else
		applyTransformation();
	applyTransformToChildren();
	mainCameraReference = NULL;
}

void GameObject::setPosition(float x, float y, float z){
	posx = x;
	posy = y;
	posz = z;
	if (parentMatrix != NULL)
		applyTransformation(parentMatrix);
	else
		applyTransformation();
	applyTransformToChildren();
}

void GameObject::rotate(float x, float y, float z){
	rotx += x;
	roty += y;
	rotz += z;
	if (parentMatrix != NULL)
		applyTransformation(parentMatrix);
	else
		applyTransformation();
	applyTransformToChildren();
}

void GameObject::setRotation(float x, float y, float z){
	rotx = x;
	roty = y;
	rotz = z;
	if (parentMatrix != NULL)
		applyTransformation(parentMatrix);
	else
		applyTransformation();
	applyTransformToChildren();
}

void GameObject::scale(float x, float y, float z){
	scalex = x;
	scaley = y;
	scalez = z;
	if (parentMatrix != NULL)
		applyTransformation(parentMatrix);
	else
		applyTransformation();
	applyTransformToChildren();
}

void GameObject::applyTransformToChildren(){
	for (std::vector<Mesh3D*>::iterator i = meshChildren.begin(); i != meshChildren.end(); i++){
		(*i)->applyTransformation(transformMatrix);
	}
	for (std::vector<GameObject*>::iterator i = children.begin(); i != children.end(); i++){
		(*i)->applyTransformation(transformMatrix);
		(*i)->applyTransformToChildren();
	}
}

void GameObject::draw(DxClass * renderer){
	drawableMeshes = 0;
	drawableFaces = 0;
	for (std::vector<GameObject*>::iterator i = children.begin(); i != children.end(); i++){
		if ((*i)->canBeDrawn(mainCameraReference, bspPlanesReference, drawPlanesRef)){
			(*i)->draw(renderer);
			drawableMeshes += (*i)->drawableMeshes;
			drawableFaces += (*i)->drawableFaces;
		}
	}
	for (std::vector<Mesh3D*>::iterator i = meshChildren.begin(); i != meshChildren.end(); i++){
		if ((*i)->canBeDrawn(mainCameraReference, bspPlanesReference, drawPlanesRef)){
			(*i)->draw(renderer);
			drawableMeshes++;
			drawableFaces += (*i)->faceQty;
		}
	}
}

void GameObject::loadModel(std::string filename){
	Mesh3D * meshRef;
	Assimp::Importer importer;
	const aiScene * scene = importer.ReadFile(filename, aiPrimitiveType_LINE | aiPrimitiveType_POINT | aiProcess_Triangulate | aiProcess_SortByPType | aiProcess_MakeLeftHanded);
	for (int i = 0; i < scene->mNumMeshes; i++){
		meshRef = new Mesh3D();
		Importer::get().loadMesh(meshRef, scene, i);
		meshChildren.push_back(meshRef);
	}
}

void GameObject::loadModelWithHierarchy(std::string filename){
	Assimp::Importer importer;
	const aiScene * scene = importer.ReadFile(filename, aiPrimitiveType_LINE | aiPrimitiveType_POINT | aiProcess_Triangulate | aiProcess_SortByPType | aiProcess_MakeLeftHanded);
	name = scene->mRootNode->mName.C_Str();
	loadHierarchy(scene->mRootNode, scene);
	applyTransformation();
	applyTransformToChildren();
}

/*void GameObject::loadModelWithHierarchy(std::string filename, std::string bspplanename) {
	BspPlaneName = bspplanename;
	Assimp::Importer importer;
	const aiScene * scene = importer.ReadFile(filename, aiPrimitiveType_LINE | aiPrimitiveType_POINT | aiProcess_Triangulate | aiProcess_SortByPType | aiProcess_MakeLeftHanded);
	name = scene->mRootNode->mName.C_Str();
	loadHierarchy(scene->mRootNode, scene);
	applyTransformation();
	applyTransformToChildren();
}*/

void GameObject::loadHierarchy(aiNode * node, const aiScene * scene){
	Mesh3D * meshRef;
	GameObject * gameObjectRef;
	aiMatrix4x4 position = node->mTransformation.Transpose();
	parseMatrix(m_OriginalTransform, &position);

	// Load meshes from current node
	for (int i = 0; i < node->mNumMeshes; i++){
		meshRef = new Mesh3D();
		meshRef->setName(node->mName.C_Str());
		if (meshRef->getName().find("Plane", 0) != std::string::npos) {
			meshRef->isBsp = true;
		}		
		Importer::get().loadMesh(meshRef, scene, node->mMeshes[i]);
		meshChildren.push_back(meshRef);
		faceQty += meshRef->faceQty;
		meshQuantity++;
	}
	// -----------------------------

	// Create children and tell children to load hierarchy aswell
	for (int i = 0; i < node->mNumChildren; i++){
		gameObjectRef = new GameObject();
		gameObjectRef->setName(node->mChildren[i]->mName.C_Str());
		gameObjectRef->loadHierarchy(node->mChildren[i], scene);
		add(gameObjectRef);
		faceQty += gameObjectRef->faceQty;
		meshQuantity += gameObjectRef->meshQuantity;
	}
}

void GameObject::parseMatrix(D3DXMATRIX * m1, aiMatrix4x4 * m2){
	m1->_11 = m2->a1;
	m1->_12 = m2->a2;
	m1->_13 = m2->a3;
	m1->_14 = m2->a4;

	m1->_21 = m2->b1;
	m1->_22 = m2->b2;
	m1->_23 = m2->b3;
	m1->_24 = m2->b4;

	m1->_31 = m2->c1;
	m1->_32 = m2->c2;
	m1->_33 = m2->c3;
	m1->_34 = m2->c4;

	m1->_41 = m2->d1;
	m1->_42 = m2->d2;
	m1->_43 = m2->d3;
	m1->_44 = m2->d4;

	/*
	m1[1, 0] = (*m2)[1, 0];
	m1[2, 0] = (*m2)[2, 0];
	m1[3, 0] = (*m2)[3, 0];

	m1[0, 1] = (*m2)[0, 1];
	m1[1, 1] = (*m2)[1, 1];
	m1[2, 1] = (*m2)[2, 1];
	m1[3, 1] = (*m2)[3, 1];

	m1[0, 2] = (*m2)[0, 2];
	m1[1, 2] = (*m2)[1, 2];
	m1[2, 2] = (*m2)[2, 2];
	m1[3, 2] = (*m2)[3, 2];

	m1[0, 3] = (*m2)[0, 3];
	m1[1, 3] = (*m2)[1, 3];
	m1[2, 3] = (*m2)[2, 3];
	m1[3, 3] = (*m2)[3, 3];*/
}

void GameObject::add(GameObject * object){
	object->parentMatrix = transformMatrix;
	children.push_back(object);
}

void GameObject::setActive(bool state){
	for (std::vector<GameObject*>::iterator i = children.begin(); i != children.end(); i++){
		(*i)->setActive(state);
	}
	for (std::vector<Mesh3D*>::iterator i = meshChildren.begin(); i != meshChildren.end(); i++){
		(*i)->setActive(state);
	}
}

bool GameObject::canBeDrawn(Camera * mainCamera, std::list<D3DXPLANE*> bspPlanes, bool drawPlanes){
	mainCameraReference = mainCamera;
	bspPlanesReference = bspPlanes;
	drawPlanesRef = drawPlanes;
	return true;
}

GameObject * GameObject::getChild(int index){
	return children[index];
}

Mesh3D * GameObject::getMesh(int index){
	return meshChildren[index];
}

std::list<D3DXPLANE*> GameObject::generateBsp(){
	std::list<D3DXPLANE*> bspList;
	for (std::vector<Mesh3D*>::iterator i = meshChildren.begin(); i != meshChildren.end(); i++) {
		if ((*i)->isBsp) {
			D3DXPLANE * plane = (*i)->getBspPlane();
			bspList.push_back(plane);
		}
	}
	std::list<D3DXPLANE*> childPlanes;
	for (std::vector<GameObject*>::iterator i = children.begin(); i != children.end(); i++) {
		bspList.merge((*i)->generateBsp());
	}
	return bspList;
}
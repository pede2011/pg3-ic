#include"Game.h"
#include"TMXLoader.h"
#include"GameObject.h"
#include<d3dx9.h>

#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif

enum CollisionMatrix{
	StaticToStatic,
	RigidBodyBToStaticA,
	RigidBodyAToStaticB,
	RigidbodyToRigidbody
};

Game::Game(){
	finished = false;
	drawBspPlanes = false;
}

Game::~Game(){
	rend = NULL;
}

void Game::finishGame(){
	finished = true;
}

bool Game::isFinished(){
	return finished;
}

void Game::setRenderer(DxClass * renderer){
	rend = renderer;
}

void Game::add(Entity2D * entity){
	renderList.push_back(entity);
}

void Game::add(Tilemap * map){
	map->setActive(true);
	renderList.push_back(map);
}

std::list<Entity2D*>& Game::getRenderList(){
	return renderList;
}

std::list<Sprite*>& Game::getTiles(){
	return tileList;
}

bool Game::collision(Entity2D * a, Entity2D * b, bool isTrigger){
	if (!a->isActive() || !b->isActive())
		return false;
	float overlapX = std::fmax(0.0f, std::fmin(a->getX() + fabs(a->getScaleX()) / 2.0f, b->getX() + fabs(b->getScaleX()) / 2.0f) - std::fmax(a->getX() - fabs(a->getScaleX()) / 2.0f, b->getX() - fabs(b->getScaleX()) / 2.0f));
	float overlapY = std::fmax(0.0f, std::fmin(a->getY() + fabs(a->getScaleY()) / 2.0f, b->getY() + fabs(b->getScaleY()) / 2.0f) - std::fmax(a->getY() - fabs(a->getScaleY()) / 2.0f, b->getY() - fabs(b->getScaleY()) / 2.0f));

	if (overlapX != 0 && overlapY != 0){
		//--------------------------------------------------------------------------
		// CHECK COLLISION TYPE
		//--------------------------------------------------------------------------
		CollisionMatrix collisionType;
		float aForce;
		float bForce;

		if (a->isRigidBody() && b->isRigidBody()){
			collisionType = CollisionMatrix::RigidbodyToRigidbody;
			if (a->getMass() == 0 && b->getMass() == 0)
				collisionType = CollisionMatrix::StaticToStatic;
			else{
				aForce = a->getMass() / (a->getMass() + b->getMass());
				bForce = b->getMass() / (a->getMass() + b->getMass());
			}
		}
		else if (!a->isRigidBody() && !b->isRigidBody()){
			collisionType = CollisionMatrix::StaticToStatic;
		}
		else if (!a->isRigidBody() && b->isRigidBody()){
			collisionType = CollisionMatrix::RigidBodyBToStaticA;
		}
		else if (!b->isRigidBody() && a->isRigidBody()){
			collisionType = CollisionMatrix::RigidBodyAToStaticB;
		}
		//--------------------------------------------------------------------------
		// APPLY COLLISIONS
		//--------------------------------------------------------------------------
		if (overlapY >= overlapX){ // HORIZONTAL COLLISION
			if (!isTrigger){
				if (collisionType == CollisionMatrix::RigidbodyToRigidbody){
					if (a->getX() >= b->getX()){
						a->translate(bForce * overlapX, 0);
						b->translate(-aForce * overlapX, 0);
					}
					else{
						a->translate(-bForce * overlapX, 0);
						b->translate(aForce * overlapX, 0);
					}
				}
				else if (collisionType == CollisionMatrix::RigidBodyAToStaticB){
					if (a->getX() >= b->getX())
						a->translate(overlapX, 0);
					else
						a->translate(-overlapX, 0);
				}
				else if (collisionType == CollisionMatrix::RigidBodyBToStaticA){
					if (b->getX() >= a->getX())
						b->translate(overlapX, 0);
					else
						b->translate(-overlapX, 0);
				}
				else if (collisionType == CollisionMatrix::StaticToStatic){
					return true;
				}
			}
			return true;
		}
		else{
			if (!isTrigger){ // VERTICAL COLLISION
				if (collisionType == CollisionMatrix::RigidbodyToRigidbody){
					if (a->getY() >= b->getY()){
						a->translate(0, bForce * overlapY);
						b->translate(0, -aForce * overlapY);
					}
					else{
						a->translate(0, -bForce * overlapY);
						b->translate(0, aForce * overlapY);
					}
				}
				else if (collisionType == CollisionMatrix::RigidBodyAToStaticB){
					if (a->getY() >= b->getY())
						a->translate(0, overlapY);
					else
						a->translate(0, -overlapY);
				}
				else if (collisionType == CollisionMatrix::RigidBodyBToStaticA){
					if (b->getY() >= a->getY())
						b->translate(0, overlapY);
					else
						b->translate(0, -overlapY);
				}
				else if (collisionType == CollisionMatrix::StaticToStatic){
					return true;
				}
			}
			return true;
		}
	}
	return false;
}

Texture Game::loadTexture(std::string const filename, int colorKey){
	return rend->loadTexture(filename);
}

void Game::setInput(DirectInput * directInput){
	dinput = directInput;
}

void Game::setTimer(Timer * gameTimer){
	gtimer = gameTimer;
}

DirectInput * Game::input(){
	return dinput;
}

Timer * Game::timer(){
	return gtimer;
}

bool Game::stob(std::string s){
	if (s == "true")
		return true;
	else return false;
}

std::map<int, Frame> Game::getFrameList(TMXLoader * map, std::string const mapName, std::string const tilesetName){
	std::map<int, Frame> frameList;
	std::pair<int, Frame> frameInfo;
	double cursorx = 0;
	double cursory = 0;
	int id = 0;
	Frame frame;
	double texWidth = map->getMap(mapName)->getTileSet(tilesetName)->getImageWidth();
	double texHeight = map->getMap(mapName)->getTileSet(tilesetName)->getImageHeight();
	double tileWidth = map->getMap(mapName)->getTileSet(tilesetName)->getTileWidth();
	double tileHeight = map->getMap(mapName)->getTileSet(tilesetName)->getTileHeight();

	for (int i = 0; i < map->getMap(mapName)->getTileSet(tilesetName)->getImageHeight() / map->getMap(mapName)->getTileSet(tilesetName)->getTileHeight(); ++i){
		for (int j = 0; j < map->getMap(mapName)->getTileSet(tilesetName)->getImageWidth() / map->getMap(mapName)->getTileSet(tilesetName)->getTileWidth(); ++j){
			
			frame.u1 = cursorx / texWidth;
			frame.v1 = cursory / texHeight;
			frame.u2 = (cursorx + tileWidth) / texWidth;
			frame.v2 = cursory / texHeight;
			frame.u3 = cursorx / texWidth;
			frame.v3 = (cursory + tileHeight) / texHeight;
			frame.u4 = (cursorx + tileWidth) / texWidth;
			frame.v4 = (cursory + tileHeight) / texHeight;

			frameInfo.first = id;
			frameInfo.second = frame;
			frameList.insert(frameInfo);

			/*std::stringstream frameInfo;
			frameInfo << "Tile ID: " << id << ", UV4 Coords: X " << frame.u4 << " Y " << frame.v4 << ", Cursor position: X " << cursorx << " Y " << cursory << ", Image Width: X " << texWidth << " Y " << texHeight << "\n";
			OutputDebugString(frameInfo.str().c_str());*/

			cursorx += tileWidth;
			id++;
		}
		cursorx = 0;
		cursory += tileHeight;
	}
	return frameList;
}

Tilemap * Game::loadTilemap(std::string const mapFileName, std::string const mapName, std::string const spriteSheetFileName, std::string const layerName, std::string const tilesetName){
	Tilemap * tilemap = new Tilemap();
	TMXLoader * map = new TMXLoader();
	Texture ssTex = loadTexture(spriteSheetFileName, 0x0000FF);
	map->loadMap(mapName, mapFileName);
	std::map<int, Frame> frameList = getFrameList(map, mapName, tilesetName);
	
	double tileWidht = map->getMap(mapName)->getTileWidth();
	double tileHeight = map->getMap(mapName)->getTileHeight();
	double texWidth = tileWidht * map->getMap(mapName)->getWidth();
	double texHeight = tileHeight * map->getMap(mapName)->getHeight();
	double cursorx = tileWidht / 2;
	double cursory = texHeight - (tileHeight / 2);
	int tileID;
	std::string canCollide;
	Sprite * tile;

	for (int i = 0; i < map->getMap(mapName)->getWidth(); i++){
		for (int j = 0; j < map->getMap(mapName)->getHeight(); j++){
			tileID = map->getMap(mapName)->getTileLayer(layerName)->getTileVector()[j][i] - 1;
			canCollide = map->getMap(mapName)->getTileSet(tilesetName)->getTile(tileID)->getProperty("collides");
			
			tile = new Sprite();
			tile->setTexture(ssTex);
			tile->setCoords(frameList[tileID].u1, frameList[tileID].v1, frameList[tileID].u2, frameList[tileID].v2, frameList[tileID].u3, frameList[tileID].v3, frameList[tileID].u4, frameList[tileID].v4);

			tile->scale(tileWidht, tileHeight);
			tile->setPosition(cursorx, cursory);
			tilemap->addTile(tile, canCollide);
			tile->setActive(false);
			tileList.push_back(tile);

			cursory -= tileHeight;
		}
		cursorx += tileWidht;
		cursory = texHeight - (tileHeight / 2);
	}

	delete map;
	return tilemap;
}

void Game::setMainCamera(Camera * camera, float FieldOfView, float NearClippingPlane, float FarClippingPlane){
	mainCamera = camera;
	camera->SetProperties(FieldOfView, NearClippingPlane, FarClippingPlane);
	camera->Init(rend);
}

Camera * Game::getMainCamera(){
	return mainCamera;
}

void Game::switchBspPlaneDraw(){
	if (drawBspPlanes == true)
		drawBspPlanes = false;
	else if (drawBspPlanes == false)
		drawBspPlanes = true;
}

bool Game::bspDrawState(){
	return drawBspPlanes;
}

std::list<D3DXPLANE*> Game::getBspPlanes() {
	return bspPlanes;
}

void Game::generateBspPlanes(GameObject * scene) {
	bspPlanes = scene->generateBsp();
}

void Game::clearBspList() {
	bspPlanes.clear();
}
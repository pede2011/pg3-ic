#include"DxClass.h"
#include"pg2_indexbuffer.h"
#include"pg2_vertexbuffer.h"
#include<d3dx9.h>

DxClass::DxClass(){
	d3do = Direct3DCreate9(D3D_SDK_VERSION);
	currentIB = NULL;
	currentVB = NULL;
}

bool DxClass::init(HWND hWnd){
	D3DPRESENT_PARAMETERS d3dpp;
	d3dpp.AutoDepthStencilFormat = D3DFMT_D24S8;
	d3dpp.BackBufferCount = 1;
	d3dpp.BackBufferFormat = D3DFMT_X8R8G8B8;
	d3dpp.BackBufferWidth = 640;
	d3dpp.BackBufferHeight = 480;
	d3dpp.Windowed = true;
	d3dpp.EnableAutoDepthStencil = D3DFMT_D24S8;
	d3dpp.Flags = 0;
	d3dpp.FullScreen_RefreshRateInHz = D3DPRESENT_RATE_DEFAULT;
	d3dpp.hDeviceWindow = hWnd;
	d3dpp.MultiSampleQuality = 0;
	d3dpp.MultiSampleType = D3DMULTISAMPLE_NONE;
	d3dpp.PresentationInterval = D3DPRESENT_INTERVAL_IMMEDIATE;
	d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;

	d3do->CreateDevice(D3DADAPTER_DEFAULT,
		D3DDEVTYPE_HAL,
		hWnd,
		D3DCREATE_HARDWARE_VERTEXPROCESSING | D3DCREATE_PUREDEVICE,
		&d3dpp,
		&d3dd);

	d3dd->SetRenderState(D3DRS_LIGHTING, FALSE);
	d3dd->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	d3dd->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
	d3dd->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	d3dd->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
	d3dd->SetRenderState(D3DRS_CULLMODE, D3DCULL_CW);
	d3dd->SetRenderState(D3DRS_ZENABLE, TRUE);
	//d3dd->SetRenderState(D3DRS_FILLMODE, D3DFILL_WIREFRAME);

	/*D3DVIEWPORT9 viewport;
	d3dd->GetViewport(&viewport);
	float vWidht = static_cast<float>(viewport.Width);
	float vHeight = static_cast<float>(viewport.Height);

	D3DXMATRIX projectionMatrix;
	//D3DXMatrixOrthoLH(&projectionMatrix, vWidht, vHeight, -1.0f, 1.0f);
	D3DXMatrixPerspectiveFovLH(&projectionMatrix, D3DXToRadian(90), vWidht / vHeight, 1, 3000);
	d3dd->SetTransform(D3DTS_PROJECTION, &projectionMatrix);*/

	vbuffer = new VertexBuffer(d3dd, sizeof(VERTEX), CUSTOMFVF);
	sbuffer = new VertexBuffer(d3dd, sizeof(UVVERTEX), UVFVF);

	return true;
}

void DxClass::clear(){
	d3dd->Clear(0,
		0,
		D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER,
		D3DCOLOR_XRGB(0, 0, 255),
		1.0f,
		0);
}

void DxClass::beginDraw(){
	d3dd->BeginScene();
}

void DxClass::draw(VERTEX shape[], D3DPRIMITIVETYPE primitiveType, unsigned int vertexCount){
	vbuffer->bind();
	vbuffer->draw(shape, primitiveType, vertexCount);
	vbuffer->flush();
}

void DxClass::draw(UVVERTEX sprite[]){
	sbuffer->bind();
	sbuffer->draw(sprite, D3DPT_TRIANGLESTRIP, 4);
	vbuffer->flush();
}

void DxClass::draw3d(D3DPRIMITIVETYPE primitiveType){
	int primCount;
	if (primitiveType == D3DPT_POINTLIST){
		primCount = currentIB->indexCount();
	}
	else if (primitiveType == D3DPT_LINELIST){
		primCount = currentIB->indexCount() / 2;
	}
	else if (primitiveType == D3DPT_LINESTRIP){
		primCount = currentIB->indexCount() - 1;
	}
	else if (primitiveType == D3DPT_TRIANGLELIST){
		primCount = currentIB->indexCount() / 3;
	}
	else if (primitiveType == D3DPT_TRIANGLESTRIP){
		primCount = currentIB->indexCount() - 2;
	}
	else if (primitiveType == D3DPT_TRIANGLEFAN){
		primCount = currentIB->indexCount() - 2;
	}

	d3dd->DrawIndexedPrimitive(primitiveType, 0, 0, currentVB->vertexCount(), 0, primCount);
}

void DxClass::endDraw(){
	d3dd->EndScene();
	d3dd->Present(0, 0, 0, 0);
}

DxClass::~DxClass(){
	delete vbuffer;
	delete sbuffer;
	vbuffer = NULL;
	sbuffer = NULL;
	d3dd = NULL;
	d3do = NULL;
}

void DxClass::cleanup(){
	d3do->Release();
	d3dd->Release();
}

void DxClass::setMatrix(MatrixType matrixType, D3DXMATRIX * matrix){
	d3dd->SetTransform(D3DTS_WORLD, matrix);
}

void DxClass::setTexture(Texture &tex){
	d3dd->SetTexture(0, tex);
}

Texture DxClass::loadTexture(std::string const &filename, int colorKey){
	Texture tex = NULL;
	HRESULT hr = D3DXCreateTextureFromFileEx(d3dd, filename.c_str(), 0, 0, 0, 0,
		D3DFMT_UNKNOWN, D3DPOOL_MANAGED, D3DX_FILTER_NONE, D3DX_FILTER_NONE, colorKey, NULL, NULL, &tex);
	if (hr != D3D_OK){
		return NULL;
	}
	else{
		textures.push_back(tex);
		return tex;
	}
}

void DxClass::loadIdentity(){
	D3DXMATRIX ident;
	D3DXMatrixIdentity(&ident);

	D3DXVECTOR3 UP(0, 1, 0);
	D3DXVECTOR3 Forward(0, 0, -1);
	D3DXVECTOR3 Position(0, 0, 0);

	D3DXMatrixLookAtLH(&ident, &Forward, &Position, &UP);
	setProjectionMatrix(&ident);
}

void DxClass::setProjectionMatrix(D3DXMATRIX * matrix){
	d3dd->SetTransform(D3DTS_VIEW, matrix);

	//d3dd->GetTransform(D3DTS_VIEW, matrix);
	//d3dd->GetTransform(D3DTS_PROJECTION, matrix);
}

void DxClass::getProjectionMatrix(D3DXMATRIX * output){
	d3dd->GetTransform(D3DTS_PROJECTION, output);
}

void DxClass::getViewMatrix(D3DXMATRIX * output){
	d3dd->GetTransform(D3DTS_VIEW, output);
}

IDirect3DDevice9 * DxClass::getDevice(){
	return d3dd;
}

void DxClass::setCurrentIndexBuffer(IndexBuffer* buffer){
	this->currentIB = buffer;
}

void DxClass::setCurrentVertexBuffer(VertexBuffer3D * buffer){
	this->currentVB = buffer;
}

VertexBuffer3D* DxClass::CreateVertexBuffer3D(size_t uiVertexSize, unsigned int uiFVF){
	return new VertexBuffer3D(this, this->d3dd, uiVertexSize, uiFVF);
}

IndexBuffer* DxClass::CreateIndexBuffer(){
	return new IndexBuffer(this, this->d3dd);
}
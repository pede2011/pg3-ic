#ifndef IMPORTER_H
#define IMPORTER_H

#include"DxClass.h"
#include<iostream>

class Mesh3D;
struct aiScene;

class GN Importer{
public:
	static Importer& get(){
		static Importer instance;
		return instance;
	}
	void loadMesh(Mesh3D * targetMesh, std::string filename);
	void loadMesh(Mesh3D * targetMesh, const aiScene * scene, int index);
private:
	Importer();
};

#endif
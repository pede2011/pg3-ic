﻿#include "Camera.h"
#include "DxClass.h"
#include "Entity3D.h"
#include <d3dx9.h>
#include <math.h>

Camera::Camera()
	/*m_Position(0.0f,0.0f,0.0f), //Set position to 0,0,0
	m_LookAt(0.0f,0.0f,1.0f), //Set look at to 0,0,1
	m_Right(1.0f,0.0f,0.0f), //Set right to 1,0,0
	m_Up(0.0f,1.0f,0.0f), //Set up to 0,1,0
	m_RotateAroundUp(0.0f),
	m_RotateAroundRight(0.0f),
	m_RotateAroundLookAt(0.0f)*/
{
	m_Position = new D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	m_LookAt = new D3DXVECTOR3(0.0f, 0.0f, 1.0f);
	m_Right = new D3DXVECTOR3(1.0f, 0.0f, 0.0f);
	m_Up = new D3DXVECTOR3(0.0f, 1.0f, 0.0f);
	m_MatView = new D3DXMATRIX();
	D3DXMatrixIdentity(m_MatView);
	m_RotateAroundUp = 0.0f;
	m_RotateAroundRight = 0.0f;
	m_RotateAroundLookAt = 0.0f;
	m_bChanged = true;
	nearPlane = 1;
	farPlane = 3000;
	fov = 90;
	m_planes = new D3DXPLANE[6];
}
//----------------------------------------------------------------
void Camera::SetProperties(float fieldOfView, float nearClippingPlane, float farClippingPlane){
	fov = fieldOfView;
	farPlane = farClippingPlane;
	nearPlane = nearClippingPlane;
}
//----------------------------------------------------------------
bool Camera::Init(DxClass* pkRenderer){
	m_pkRenderer = pkRenderer;

	D3DVIEWPORT9 viewport;
	pkRenderer->getDevice()->GetViewport(&viewport);
	float vWidht = static_cast<float>(viewport.Width);
	float vHeight = static_cast<float>(viewport.Height);

	D3DXMATRIX projectionMatrix;
	//D3DXMatrixOrthoLH(&projectionMatrix, vWidht, vHeight, -1.0f, 1.0f);
	D3DXMatrixPerspectiveFovLH(&projectionMatrix, D3DXToRadian(fov), vWidht / vHeight, nearPlane, farPlane);
	pkRenderer->getDevice()->SetTransform(D3DTS_PROJECTION, &projectionMatrix);

	return true;
}
Camera::~Camera()
{
	delete m_Position;
	delete m_LookAt;
	delete m_Right;
	delete m_Up;
	delete m_MatView;
	delete m_planes;
	// Deletes
}
//----------------------------------------------------------------
void Camera::SetPosition(float fX, float fY, float fZ)
{
	delete m_Position;
	m_Position = new D3DXVECTOR3(fX, fY, fZ);
	m_bChanged= true;
}
//----------------------------------------------------------------
void Camera::MoveForward(float Dist)
{
	*m_Position += Dist * *m_LookAt;
	m_bChanged = true;
}
//----------------------------------------------------------------
void Camera::MoveRight(float Dist)
{
	*m_Position += Dist* *m_Right;
	m_bChanged = true;
}
//----------------------------------------------------------------
void Camera::MoveUp(float Dist)
{
	*m_Position += Dist* *m_Up;
	m_bChanged = true;
}
//----------------------------------------------------------------
void Camera::MoveInDirection(float Dist, float fDirectionX, float fDirectionY, float fDirectionZ)
{
	delete m_Position;
	*m_Position += Dist* *new D3DXVECTOR3(fDirectionX, fDirectionY, fDirectionZ);
	m_bChanged = true;
}
//----------------------------------------------------------------
void Camera::RotateDown(float Angle)
{
	m_RotateAroundRight = Angle;
	m_bChanged = true;
}
//----------------------------------------------------------------
void Camera::RotateRight(float Angle)
{
	m_RotateAroundUp = Angle;
	m_bChanged = true;
}
//----------------------------------------------------------------
void Camera::Roll(float Angle)
{
	m_RotateAroundLookAt += Angle;
	m_bChanged = true;
}
//----------------------------------------------------------------
void Camera::Update(DxClass * renderer)
{
	if(m_bChanged)
	{
		//Matrices to store the transformations about our axes
		D3DXMATRIX MatTotal;
		D3DXMATRIX MatRotateAroundRight;
		D3DXMATRIX MatRotateAroundUp;
		D3DXMATRIX MatRotateAroundLookAt;
		//Get the matrix for each rotation
		D3DXMatrixRotationAxis(&MatRotateAroundRight, m_Right, m_RotateAroundRight);
		D3DXMatrixRotationAxis(&MatRotateAroundUp, m_Up, m_RotateAroundUp);
		D3DXMatrixRotationAxis(&MatRotateAroundLookAt, m_LookAt, m_RotateAroundLookAt);
		//Combine the transformations into one matrix
		D3DXMatrixMultiply(&MatTotal, &MatRotateAroundUp, &MatRotateAroundRight);
		D3DXMatrixMultiply(&MatTotal, & MatRotateAroundLookAt, & MatTotal);
		//Transforms two vectors by our matrix and computes the third by
		//cross product
		D3DXVec3TransformCoord(m_Right, m_Right, &MatTotal);
		D3DXVec3TransformCoord(m_Up, m_Up, &MatTotal);
		D3DXVec3Cross(m_LookAt, m_Right, m_Up);
		//Check to ensure vectors are perpendicular
		if (fabs(D3DXVec3Dot(m_Up, m_Right)) > 0.01)
		{
			//If theyre not
			D3DXVec3Cross(m_Up, m_LookAt, m_Right);
		}
		//Normalize our vectors
		D3DXVec3Normalize(m_Right, m_Right);
		D3DXVec3Normalize(m_Up, m_Up);
		D3DXVec3Normalize(m_LookAt, m_LookAt);
		//Compute the bottom row of the view matrix
		float fView41,fView42,fView43;
		fView41 = -D3DXVec3Dot(m_Right, m_Position);
		fView42 = -D3DXVec3Dot(m_Up, m_Position);
		fView43 = -D3DXVec3Dot(m_LookAt, m_Position);
		//Fill in the view matrix
		delete m_MatView;
		m_MatView = new D3DXMATRIX(m_Right->x, m_Up->x, m_LookAt->x, 0.0f,
			m_Right->y, m_Up->y, m_LookAt->y, 0.0f,
			m_Right->z, m_Up->z, m_LookAt->z, 0.0f,
			fView41, fView42, fView43, 1.0f);
	}
	//Set view transform
	m_pkRenderer->loadIdentity();
	m_pkRenderer->setProjectionMatrix(m_MatView);
	//Reset update members
	m_RotateAroundRight = m_RotateAroundUp = m_RotateAroundLookAt = 0.0f;
	m_bChanged = false;



	// Build Frustum
	D3DXMATRIX projection;
	D3DXMATRIX view;
	renderer->getProjectionMatrix(&projection);
	renderer->getViewMatrix(&view);
	D3DXMATRIX mat;
	D3DXMatrixMultiply(&mat, &view, &projection);

	// Extraigo plano left
	m_planes[0].a = mat._14 + mat._11;
	m_planes[0].b = mat._24 + mat._21;
	m_planes[0].c = mat._34 + mat._31;
	m_planes[0].d = mat._44 + mat._41;

	// Extraigo plano right
	m_planes[1].a = mat._14 - mat._11;
	m_planes[1].b = mat._24 - mat._21;
	m_planes[1].c = mat._34 - mat._31;
	m_planes[1].d = mat._44 - mat._41;

	// Extraigo plano bottom
	m_planes[2].a = mat._14 + mat._12;
	m_planes[2].b = mat._24 + mat._22;
	m_planes[2].c = mat._34 + mat._32;
	m_planes[2].d = mat._44 + mat._42;

	// Extraigo plano top
	m_planes[3].a = mat._14 - mat._12;
	m_planes[3].b = mat._24 - mat._22;
	m_planes[3].c = mat._34 - mat._32;
	m_planes[3].d = mat._44 - mat._42;

	// Extraigo plano near
	m_planes[4].a = mat._13;
	m_planes[4].b = mat._23;
	m_planes[4].c = mat._33;
	m_planes[4].d = mat._43;

	// Extraigo plano far
	m_planes[5].a = mat._14 - mat._13;
	m_planes[5].b = mat._24 - mat._23;
	m_planes[5].c = mat._34 - mat._33;
	m_planes[5].d = mat._44 - mat._43;

	// normalize planes
	for (unsigned int i = 0; i<6; i++){
		D3DXPlaneNormalize(&m_planes[i], &m_planes[i]);
	}
}

bool Camera::checkBoundingSphere(float x, float y, float z, float radius){
	D3DXVECTOR3 position;
	position.x = x;
	position.y = y;
	position.z = z;

	for (int i = 0; i < 6; i++){
		if (D3DXPlaneDotCoord(&m_planes[i], &position) + radius < 0){
			return false;
		}
	}
	return true;
}

bool Camera::binarySpatialPartition(float x, float y, float z, float radius, std::list<D3DXPLANE*> planes){
	for (std::list<D3DXPLANE*>::iterator i = planes.begin(); i != planes.end(); i++) {
		if (objectNotInCameraPlane(x, y, z, radius, (*i)))
			return true;
	}
	return false;
}
//objectNotInCameraPlane
//If false, it means that both the camera and the object, are in the same
//side of said plane
bool Camera::objectNotInCameraPlane(float x, float y, float z, float radius, D3DXPLANE * plane) {
	float cameraResult = D3DXPlaneDotCoord(plane, m_Position);

	D3DXVECTOR3 boundingCube[8] = {D3DXVECTOR3((x - radius), (y - radius), (z - radius)),
								   D3DXVECTOR3((x + radius), (y - radius), (z - radius)),
								   D3DXVECTOR3((x - radius), (y + radius), (z - radius)),
								   D3DXVECTOR3((x + radius), (y + radius), (z - radius)),
								   D3DXVECTOR3((x - radius), (y - radius), (z + radius)),
								   D3DXVECTOR3((x + radius), (y - radius), (z + radius)),
								   D3DXVECTOR3((x - radius), (y + radius), (z + radius)),
								   D3DXVECTOR3((x + radius), (y + radius), (z + radius)) };

	for (int i = 0; i < 8; i++) {
		float pointResult = D3DXPlaneDotCoord(plane, &boundingCube[i]);
		
		if (signbit(cameraResult) == signbit(pointResult))
			return false;
	}
	return true;
}

float Camera::debugDotCoord(D3DXPLANE* plane){
	return D3DXPlaneDotCoord(plane, m_Position);
}
#ifndef VENTANA_H
#define VENTANA_H

#ifndef GN
#define GN __declspec(dllexport)
#else
#define GN __declspec(dllimport)
#endif

#include<Windows.h>
#include<iostream>

class GN Ventana{
private:
	HWND win;
public:
	bool CrearVentana(HINSTANCE hInstance, unsigned int w, unsigned int h);
	HWND hwnd() const;
};
#endif
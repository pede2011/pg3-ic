#ifndef DXCLASS_H
#define DXCLASS_H

#define CUSTOMFVF (D3DFVF_XYZ | D3DFVF_DIFFUSE)
#define UVFVF (D3DFVF_XYZ | D3DFVF_TEX1)

#include"VertexBuffer.h"
#include<vector>
#include<string>
class D3DXMATRIX;
class IndexBuffer;
class VertexBuffer3D;

struct GN VERTEX
{
	FLOAT x, y, z;
	DWORD color;
};

struct GN UVVERTEX
{
	FLOAT x, y, z, u, v;
};

enum GN MatrixType
{
	View = 0,
	Projection,
	World,
};

typedef IDirect3DTexture9 * Texture;

//Crear como puntero.
class GN DxClass{
public:
	DxClass();
	~DxClass();
	bool init(HWND hWnd);
	void draw(VERTEX shape[], D3DPRIMITIVETYPE primitiveType, unsigned int vertexCount);
	void draw(UVVERTEX sprite[]);
	void cleanup();
	void setMatrix(MatrixType matrixType, D3DXMATRIX * matrix);
	void setProjectionMatrix(D3DXMATRIX * matrix);
	void setTexture(Texture &tex);
	Texture loadTexture(std::string const &filename, int colorKey=0xFFFFFFFF);
	void clear();
	void beginDraw();
	void endDraw();
	void loadIdentity();
	void setCurrentIndexBuffer(IndexBuffer* buffer);
	void setCurrentVertexBuffer(VertexBuffer3D* buffer);
	void draw3d(D3DPRIMITIVETYPE primitiveType);
	VertexBuffer3D* CreateVertexBuffer3D(size_t uiVertexSize, unsigned int uiFVF);
	IndexBuffer*	CreateIndexBuffer();
	void getProjectionMatrix(D3DXMATRIX * output);
	void getViewMatrix(D3DXMATRIX * output);
private:
	IDirect3DDevice9 * d3dd;
	IDirect3D9 * d3do;
	VertexBuffer * vbuffer;
	VertexBuffer * sbuffer;
	std::vector<Texture> textures;
protected:
	IDirect3DDevice9 * getDevice();
	friend class Camera;
private:
	VertexBuffer3D* currentVB;
	IndexBuffer*	currentIB;
};
#endif
﻿#ifndef CAMERA_H
#define CAMERA_H

#ifdef CM
#define GN __declspec(dllimport)
#else
#define GN __declspec(dllexport)
#endif

#include <d3d9.h>
#include <Windows.h>
#include <list>

class DxClass;
class Entity3D;
class D3DXVECTOR3;
class D3DXMATRIX;
class D3DXPLANE;

class GN Camera{
public:
	Camera();
	~Camera();
	bool Init(DxClass* pkRenderer);

protected:
	DxClass* m_pkRenderer;
	D3DXVECTOR3 * m_Position;
	D3DXVECTOR3 * m_LookAt;
	D3DXVECTOR3 * m_Right;
	D3DXVECTOR3 * m_Up;
	bool m_bChanged;
	float m_RotateAroundUp;
	float m_RotateAroundRight;
	float m_RotateAroundLookAt;
	D3DXMATRIX* m_MatView; // View Matrix from Camera.

public:

	void SetPosition(float fX, float fY, float fZ);
	D3DXVECTOR3 * GetPosition() { return m_Position; }
	void MoveForward(float Dist);
	void MoveRight(float Dist);
	void MoveUp(float Dist);
	void MoveInDirection(float Dist, float fDirectionX, float fDirectionY, float fDirectionZ);
	void RotateDown(float Angle);
	void RotateRight(float Angle);
	void Roll(float Angle);
	float debugDotCoord(D3DXPLANE* plane);

protected:
	void Update(DxClass * renderer);
	friend class Engine;
	void SetProperties(float FieldOfView, float nearClippingPlane, float farClippingPlane);
private:
	bool objectNotInCameraPlane(float x, float y, float z, float radius, D3DXPLANE * plane);
	friend class Scene;
	friend class Game;
	friend class Mesh3D;
	D3DXPLANE * m_planes;
	bool checkBoundingSphere(float x, float y, float z, float radius);
	bool binarySpatialPartition(float x, float y, float z, float radius, std::list<D3DXPLANE*> planes);
	float nearPlane;
	float farPlane;
	float fov;
};

#endif
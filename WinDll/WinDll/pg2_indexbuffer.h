
//---------------------------------------------------------------------------
#ifndef PG2_INDEXBUFFER_H
#define PG2_INDEXBUFFER_H
//---------------------------------------------------------------------------
#ifdef IB
#define GN __declspec(dllimport)
#else
#define GN __declspec(dllexport)
#endif
//---------------------------------------------------------------------------
#include <d3d9.h>
#include <d3dx9.h>
#include <cassert>
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
class DxClass;
//---------------------------------------------------------------------------
class GN IndexBuffer
{
public:
	IndexBuffer (DxClass* rkRenderer, IDirect3DDevice9* pkDevice);
	~IndexBuffer ();

	void bind ();
	void setIndexData (const unsigned short* pausIndices, size_t uiIndexCount);
	
	size_t indexCount () const;

private:
	size_t m_uiIndexCount;

	D3DPRIMITIVETYPE m_ePrimitiveType;
	LPDIRECT3DINDEXBUFFER9 m_IndexBuffer;
	IDirect3DDevice9* m_pkDevice;
	DxClass* m_rkRenderer;
};
//---------------------------------------------------------------------------
#include "pg2_indexbuffer.inl"
//---------------------------------------------------------------------------
#endif  // PG2_INDEXBUFFER_H
//---------------------------------------------------------------------------

#include"Entity2D.h"
#include<d3dx9.h>

Entity2D::Entity2D(){
	active = true;
	isRigidBodyEnabled = false;
	_mass = 1.0f;
	posx = 0;
	posy = 0;
	rotation = 0;
	scalex = 1;
	scaley = 1;
	transformMatrix = new D3DXMATRIX();
	applyTransformation();
	isBsp = false;
	isDebugBsp = false;
	name = "";
}

Entity2D::~Entity2D(){
	delete transformMatrix;
	transformMatrix = NULL;
}

void Entity2D::translate(float x, float y){
	posx += x;
	posy += y;
	applyTransformation();
}

void Entity2D::setPosition(float x, float y){
	posx = x;
	posy = y;
	applyTransformation();
}

void Entity2D::rotate(float z){
	rotation = z;
	applyTransformation();
}

void Entity2D::scale(float x, float y){
	scalex = x;
	scaley = y;
	applyTransformation();
}

void Entity2D::applyTransformation(){
	D3DXMATRIX trans;
	D3DXMATRIX rot;
	D3DXMATRIX scl;
	D3DXMatrixTranslation(&trans, posx, posy, 0);
	D3DXMatrixRotationZ(&rot, D3DXToRadian(rotation));
	D3DXMatrixScaling(&scl, scalex, scaley, 1);

	D3DXMatrixIdentity(transformMatrix);
	D3DXMatrixMultiply(transformMatrix, &trans, transformMatrix);
	D3DXMatrixMultiply(transformMatrix, &rot, transformMatrix);
	D3DXMatrixMultiply(transformMatrix, &scl, transformMatrix);
}

void Entity2D::setRigidBody(bool enableRigidBody, float mass){
	isRigidBodyEnabled = enableRigidBody;
	_mass = mass;
	if (_mass < 0)
		mass = 0;
}

float Entity2D::getMass(){
	return _mass;
}

bool Entity2D::isRigidBody(){
	return isRigidBodyEnabled;
}

float Entity2D::getX() const{
	return posx;
}

float Entity2D::getY() const{
	return posy;
}

float Entity2D::getRotation() const{
	return rotation;
}

float Entity2D::getScaleX() const{
	return scalex;
}

float Entity2D::getScaleY() const{
	return scaley;
}

std::string Entity2D::getName(){
	return name;
}

void Entity2D::setName(std::string newName){
	name = newName;
}

bool Entity2D::isActive(){
	return active;
}

void Entity2D::setActive(bool state){
	active = state;
}

bool Entity2D::isBspPlane(){
	return isBsp;
}

bool Entity2D::isDebugBspPlane(){
	return isDebugBsp;
}
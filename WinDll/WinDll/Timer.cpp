#include"Timer.h"

Timer::Timer(){
	_deltaTime = 0;
	_fps = 0;
	fpsCounter = 0;
	measureSample = 0;
}

void Timer::firtsMeasure(){
	_deltaTime = 0;
	_fps = 0;
	fpsCounter = 0;
	measureSample = 0;
	QueryPerformanceFrequency(&frequency);
	QueryPerformanceCounter(&performanceCounter1);
}

void Timer::measure(){
	QueryPerformanceCounter(&performanceCounter2);
	_deltaTime = static_cast<double>((performanceCounter2.QuadPart - performanceCounter1.QuadPart) * 1000.0f / frequency.QuadPart);
	measureSample += _deltaTime;
	fpsCounter++;
	performanceCounter1 = performanceCounter2;
	while (measureSample >= 1000){
		measureSample -= 1000;
		_fps = fpsCounter;
		fpsCounter = 0;
	}
}

float Timer::deltaTime(){
	return static_cast<float>(_deltaTime);
}

int Timer::fps(){
	return _fps;
}
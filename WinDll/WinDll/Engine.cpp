#include"Engine.h"
#include<d3dx9.h>
#include<list>

#define _CRT_SECURE_NO_WARNINGS

Engine::Engine(HINSTANCE hInstance, unsigned int w, unsigned int h){
	win = new Ventana();
	winhInstance = hInstance;
	winW = w;
	winH = h;
	renderer = new DxClass();
	dInput = new DirectInput();
	timer = new Timer();
}

Engine::~Engine(){
	dInput->deinit();
	game->Shutdown();
	delete dInput;
	delete win;
	delete renderer;
	delete game;
	delete timer;
	win = NULL;
	renderer = NULL;
	timer = NULL;
}

bool Engine::init(Game * g){
	game = g;

	if (!win->CrearVentana(winhInstance, winW, winH)){
		return false;
	}

	if (!renderer->init(win->hwnd())){
		return false;
	}

	if (!dInput->init(winhInstance, win->hwnd())){
		return false;
	}
	game->setRenderer(renderer);
	game->setInput(dInput);
	game->setTimer(timer);
	game->Start();

	return true;
}

void Engine::run(){
	timer->firtsMeasure();

	// Render counter/s
	int prevMeshQty = 0;
	int meshQty = 0;
	int prevFacesQty = 0;
	int facesQty = 0;
	int meshTotalQty = 0;
	int faceTotalQty = 0;
	std::list<D3DXPLANE*> bsplist;
	//std::ostringstream entitynames;
	// ----------------

	MSG m;
	while (!game->isFinished()){
		timer->measure();
		dInput->reacquire();
		renderer->clear();
		game->Update();
		game->getMainCamera()->Update(renderer);

		// Render counter/s
		meshQty = 0;
		facesQty = 0;
		meshTotalQty = 0;
		faceTotalQty = 0;
		bsplist = game->getBspPlanes();
		//entitynames.clear();
		// ----------------

		renderer->beginDraw();
		for (std::list<Entity2D*>::iterator i = game->getRenderList().begin(); i != game->getRenderList().end(); i++){
			if ((*i)->isActive() && (*i)->canBeDrawn(game->getMainCamera(), game->getBspPlanes(), game->bspDrawState())){
				(*i)->draw(renderer);

				// Render counter/s
				facesQty += (*i)->drawableFaces;
				meshQty += (*i)->drawableMeshes;
				faceTotalQty += (*i)->faceQty;
				meshTotalQty += (*i)->meshQuantity;
				// ----------------
				// Object Names
				//entitynames << "\n";
				//entitynames << (*i)->getName().c_str();
				// ------------
			}
		}
		renderer->endDraw();

		// Console Output
		if (facesQty != prevFacesQty || meshQty != prevMeshQty){
			system("cls");
			std::ostringstream oss;
			oss << "Numero total de meshes: ";
			oss << meshTotalQty;
			oss << "\n";
			oss << "Numero de meshes en pantalla: ";
			oss << meshQty;
			oss << "\n";
			oss << "Numero total de faces: ";
			oss << faceTotalQty;
			oss << "\n";
			oss << "Numero de faces en pantalla: ";
			oss << facesQty;
			oss << "\n";
			//oss << "BSP Draw State: \n";
			//oss << game->bspDrawState();
			oss << "\nCamera position - X:";
			oss << game->getMainCamera()->GetPosition()->x;
			oss << "\nCamera position - Y:";
			oss << game->getMainCamera()->GetPosition()->x;
			oss << "\nCamera position - Z:";
			oss << game->getMainCamera()->GetPosition()->x;
			//oss << "Nombres de las entidades:";
			//oss << entitynames.str().c_str();
			printf(oss.str().c_str());
			prevFacesQty = facesQty;
			prevMeshQty = meshQty;
		}
		// -------------

		if (PeekMessage(&m, NULL, 0, 0, PM_REMOVE) != 0) {
			if (m.message == WM_QUIT) {
				game->finishGame();
			}
			TranslateMessage(&m);
			DispatchMessage(&m);
		}
	}

	renderer->cleanup();
}

void Engine::RedirectIOToConsole(){
	int hConHandle;
	long lStdHandle;
	CONSOLE_SCREEN_BUFFER_INFO coninfo;
	FILE *fp;
	AllocConsole();
	GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE),
		&coninfo);
	coninfo.dwSize.Y = MAX_CONSOLE_LINES;
	SetConsoleScreenBufferSize(GetStdHandle(STD_OUTPUT_HANDLE),
		coninfo.dwSize);
	lStdHandle = (long)GetStdHandle(STD_OUTPUT_HANDLE);
	hConHandle = _open_osfhandle(lStdHandle, _O_TEXT);
	fp = _fdopen(hConHandle, "w");
	*stdout = *fp;
	setvbuf(stdout, NULL, _IONBF, 0);
	lStdHandle = (long)GetStdHandle(STD_INPUT_HANDLE);
	hConHandle = _open_osfhandle(lStdHandle, _O_TEXT);
	fp = _fdopen(hConHandle, "r");
	*stdin = *fp;
	setvbuf(stdin, NULL, _IONBF, 0);
	lStdHandle = (long)GetStdHandle(STD_ERROR_HANDLE);
	hConHandle = _open_osfhandle(lStdHandle, _O_TEXT);
	fp = _fdopen(hConHandle, "w");
	*stderr = *fp;
	setvbuf(stderr, NULL, _IONBF, 0);
	std::ios::sync_with_stdio();
}
#ifndef QUAD_H
#define QUAD_H

#include"Entity2D.h"

class GN Quad : public Entity2D{
public:
	Quad();
	~Quad();
	void draw(DxClass * renderer);
	void layer(int layer);
	bool canBeDrawn(Camera * mainCamera);
private:
	VERTEX * shape;
};

#endif
#include"Mesh3D.h"
#include"pg2_indexbuffer.h"
#include"pg2_vertexbuffer.h"
#include"Camera.h"
#include<d3dx9.h>

Mesh3D::Mesh3D(){
	firstDraw = false;
	tex = NULL;
	faceQty = 0;
	boundingSphereCentre = new D3DXVECTOR3();
	boundingSphereWorld = new D3DXVECTOR3();
}

Mesh3D::~Mesh3D(){
	if (tex)
		//delete tex;
	if (ib)
		delete ib;
	if (vb)
		delete vb;
	if (meshStructure)
		delete meshStructure;
	if (meshStructureUV)
		delete meshStructureUV;
	if (meshIndexes)
		delete meshIndexes;
	if (boundingSphereCentre)
		delete boundingSphereCentre;
	if (boundingSphereWorld)
		delete boundingSphereWorld;
	tex = NULL;
	ib = NULL;
	vb = NULL;
	meshStructure = NULL;
	meshStructureUV = NULL;
	meshIndexes = NULL;
	boundingSphereRadius = 0;
	originalSphereRadius = 0;
	boundingSphereCentre = NULL;
	boundingSphereWorld = NULL;
	faceQty = 0;
}

bool Mesh3D::canBeDrawn(Camera * mainCamera, std::list<D3DXPLANE*> bspPlanes, bool drawPlanes){
	if (!drawPlanes && isBsp)
		return false;
	else if (drawPlanes && isBsp)
		return true;

	// Binary Spatial Partitioning
	if (bspPlanes.size() != 0 && !isBsp) {	

		// MESH-CENTERED BOUNDING SPHERE
		if (mainCamera->binarySpatialPartition(boundingSphereWorld->x, boundingSphereWorld->y, boundingSphereWorld->z, boundingSphereRadius, bspPlanes))
			return false;
	}

	// MESH-CENTERED FRUSTUM CULLING
	return mainCamera->checkBoundingSphere(boundingSphereWorld->x, boundingSphereWorld->y, boundingSphereWorld->z, boundingSphereRadius);
}

void Mesh3D::draw(DxClass * renderer){
	if (!firstDraw){
		if (textured){
			vb = renderer->CreateVertexBuffer3D(sizeof(UVVERTEX), UVFVF);
			vb->setVertexData((void*)meshStructureUV, vertexCount);
		}
		else{
			vb = renderer->CreateVertexBuffer3D(sizeof(VERTEX), CUSTOMFVF);
			vb->setVertexData((void*)meshStructure, vertexCount);
		}
		ib = renderer->CreateIndexBuffer();
		tex = renderer->loadTexture(texturePath);
		ib->setIndexData(meshIndexes, indexCount);
		type = D3DPT_TRIANGLELIST;
		firstDraw = true;
	}

	if (textured)
		renderer->setTexture(tex);

	vb->bind();
	ib->bind();
	renderer->setMatrix(World, transformMatrix);
	renderer->draw3d(type);
}

void Mesh3D::setTexture(Texture texture){
	tex = texture;
}

void Mesh3D::setStructure(UVVERTEX * structure, USHORT * indexes, size_t vtxCount, size_t indCount){
	textured = true;
	meshStructureUV = structure;
	meshIndexes = indexes;
	vertexCount = vtxCount;
	indexCount = indCount;
	calculateSphereRadius(structure, vtxCount);
}

void Mesh3D::setStructure(VERTEX * structure, USHORT * indexes, size_t vtxCount, size_t indCount){
	textured = false;
	meshStructure = structure;
	meshIndexes = indexes;
	vertexCount = vtxCount;
	indexCount = indCount;
	calculateSphereRadius(structure, vtxCount);
}

void Mesh3D::scale(float x, float y, float z){
	scalex = x;
	scaley = y;
	scalez = z;

	if (x >= y && x >= z)
		boundingSphereRadius = originalSphereRadius * x;
	else if (y >= x && y >= z)
		boundingSphereRadius = originalSphereRadius * y;
	else if (z >= x && z >= y)
		boundingSphereRadius = originalSphereRadius * z;

	if (boundingSphereRadius < 0)
		boundingSphereRadius *= -1;

	applyTransformation();
}

void Mesh3D::calculateSphereRadius(VERTEX * vertices, size_t count){

	// METHOD 1:
	D3DXVECTOR3 minVertex = D3DXVECTOR3(FLT_MAX, FLT_MAX, FLT_MAX);
	D3DXVECTOR3 maxVertex = D3DXVECTOR3(-FLT_MAX, -FLT_MAX, -FLT_MAX);

	for (int i = 0; i < count; i++){
		minVertex.x = min(minVertex.x, vertices[i].x);
		minVertex.y = min(minVertex.y, vertices[i].y);
		minVertex.z = min(minVertex.z, vertices[i].z);
		maxVertex.x = max(maxVertex.x, vertices[i].x);
		maxVertex.y = max(maxVertex.y, vertices[i].y);
		maxVertex.z = max(maxVertex.z, vertices[i].z);
	}

	float distX = (maxVertex.x - minVertex.x) / 2.0f;
	float distY = (maxVertex.y - minVertex.y) / 2.0f;
	float distZ = (maxVertex.z - minVertex.z) / 2.0f;

	boundingSphereCentre->x = maxVertex.x - distX;
	boundingSphereCentre->y = maxVertex.y - distY;
	boundingSphereCentre->z = maxVertex.z - distZ;

	boundingSphereRadius = sqrt(distX*distX + distY*distY + distZ*distZ) / 2.0f;

}

void Mesh3D::calculateSphereRadius(UVVERTEX * vertices, size_t count){

	// METHOD 1:
	D3DXVECTOR3 minVertex = D3DXVECTOR3(FLT_MAX, FLT_MAX, FLT_MAX);
	D3DXVECTOR3 maxVertex = D3DXVECTOR3(-FLT_MAX, -FLT_MAX, -FLT_MAX);

	for (int i = 0; i < count; i++){
		minVertex.x = min(minVertex.x, vertices[i].x);
		minVertex.y = min(minVertex.y, vertices[i].y);
		minVertex.z = min(minVertex.z, vertices[i].z);
		maxVertex.x = max(maxVertex.x, vertices[i].x);
		maxVertex.y = max(maxVertex.y, vertices[i].y);
		maxVertex.z = max(maxVertex.z, vertices[i].z);
	}

	float distX = (maxVertex.x - minVertex.x) / 2.0f;
	float distY = (maxVertex.y - minVertex.y) / 2.0f;
	float distZ = (maxVertex.z - minVertex.z) / 2.0f;

	boundingSphereCentre->x = maxVertex.x - distX;
	boundingSphereCentre->y = maxVertex.y - distY;
	boundingSphereCentre->z = maxVertex.z - distZ;

	boundingSphereRadius = sqrt(distX*distX + distY*distY + distZ*distZ) / 2.0f;
}

void Mesh3D::setTexturePath(std::string path){
	texturePath = path;
}

D3DXPLANE * Mesh3D::getBspPlane(){
	D3DXPLANE * plane = new D3DXPLANE();
	D3DXPlaneFromPoints(plane, planeP1, planeP2, planeP3);
	D3DXPlaneNormalize(plane, plane);
	 return plane;
}

void Mesh3D::applyTransformation(){
	D3DXMATRIX trans;
	D3DXMATRIX mRotZ;
	D3DXMATRIX mRotX;
	D3DXMATRIX mRotY;
	D3DXMATRIX rot;
	D3DXMATRIX scl;
	D3DXMatrixTranslation(&trans, posx, posy, posz);

	D3DXMatrixRotationZ(&mRotZ, D3DXToRadian(rotz));
	D3DXMatrixRotationY(&mRotY, D3DXToRadian(roty));
	D3DXMatrixRotationX(&mRotX, D3DXToRadian(rotx));

	D3DXMatrixIdentity(&rot);
	D3DXMatrixMultiply(&rot, &mRotZ, &mRotY);
	D3DXMatrixMultiply(&rot, &mRotX, &rot);

	D3DXMatrixScaling(&scl, scalex, scaley, scalez);

	D3DXMatrixIdentity(transformMatrix);
	D3DXMatrixMultiply(transformMatrix, &trans, transformMatrix);
	D3DXMatrixMultiply(transformMatrix, &rot, transformMatrix);
	D3DXMatrixMultiply(transformMatrix, &scl, transformMatrix);

	D3DXMatrixMultiply(transformMatrix, m_OriginalTransform, transformMatrix);

	boundingSphereWorld->x = 0;
	boundingSphereWorld->y = 0;
	boundingSphereWorld->z = 0;
	D3DXVec3TransformCoord(boundingSphereWorld, boundingSphereCentre, m_OriginalTransform);

	//transformMatrix = &(rot*trans*scl);
	//transformMatrix = &(trans*rot*scl);
}

void Mesh3D::applyTransformation(D3DXMATRIX * parentMatrix){
	D3DXMATRIX trans;
	D3DXMATRIX mRotZ;
	D3DXMATRIX mRotX;
	D3DXMATRIX mRotY;
	D3DXMATRIX rot;
	D3DXMATRIX scl;
	D3DXMatrixTranslation(&trans, posx, posy, posz);

	D3DXMatrixRotationZ(&mRotZ, D3DXToRadian(rotz));
	D3DXMatrixRotationY(&mRotY, D3DXToRadian(roty));
	D3DXMatrixRotationX(&mRotX, D3DXToRadian(rotx));

	D3DXMatrixIdentity(&rot);
	D3DXMatrixMultiply(&rot, &mRotZ, &mRotY);
	D3DXMatrixMultiply(&rot, &mRotX, &rot);

	D3DXMatrixScaling(&scl, scalex, scaley, scalez);

	D3DXMatrixIdentity(transformMatrix);
	D3DXMatrixMultiply(transformMatrix, m_OriginalTransform, transformMatrix);
	D3DXMatrixMultiply(transformMatrix, &trans, transformMatrix);
	D3DXMatrixMultiply(transformMatrix, &rot, transformMatrix);
	D3DXMatrixMultiply(transformMatrix, &scl, transformMatrix);

	//D3DXMatrixMultiply(transformMatrix, m_OriginalTransform, transformMatrix);
	D3DXMatrixMultiply(transformMatrix, transformMatrix, parentMatrix);

	boundingSphereWorld->x = 0;
	boundingSphereWorld->y = 0;
	boundingSphereWorld->z = 0;
	D3DXVec3TransformCoord(boundingSphereWorld, boundingSphereCentre, parentMatrix);

	//transformMatrix = &(rot*trans*scl);
	//transformMatrix = &(trans*rot*scl);
}

//OLD

// PIVOT-CENTERED BOUNDING SPHERE
/*if (!mainCamera->binarySpatialPartition(transformMatrix->_41, transformMatrix->_42, transformMatrix->_43, boundingSphereRadius, bspPlanes))
return false;*/

// PIVOT-CENTERED FRUSTUM CULLING
//return mainCamera->checkBoundingSphere(transformMatrix->_41, transformMatrix->_42, transformMatrix->_43, boundingSphereRadius);

/* METHOD 2:
D3DXVECTOR3 minDistance;
D3DXVECTOR3 maxDistance;
float maxMagnitude = 0;
float minMagnitude = 0;
float result = 0;
for (int i = 0; i < count; i++){
result = sqrt(pow(vertices[i].x, 2) + pow(vertices[i].y, 2) + pow(vertices[i].z, 2));
if (result > maxMagnitude){
maxMagnitude = result;
if (i == 0)
minMagnitude = maxMagnitude;
maxDistance.x = vertices[i].x;
maxDistance.y = vertices[i].y;
maxDistance.z = vertices[i].z;
}
if (result <= minMagnitude){
minMagnitude = result;
minDistance.x = vertices[i].x;
minDistance.y = vertices[i].y;
minDistance.z = vertices[i].z;
}
}

D3DXVECTOR3 sphereCentre;
D3DXVec3Add(&sphereCentre, &minDistance, &maxDistance);
sphereCentre *= 0.5f;
D3DXMatrixIdentity(boundingSphereCentre);
D3DXMatrixTranslation(boundingSphereCentre, sphereCentre.x, sphereCentre.x, sphereCentre.x);

originalSphereRadius = (sqrt(pow(maxDistance.x - minDistance.x, 2) + pow(maxDistance.y - minDistance.y, 2) + pow(maxDistance.z - minDistance.z, 2))) / 2;
boundingSphereRadius = originalSphereRadius;
*/

/* METHOD 3:
float maxMagnitude = 0;
float result = 0;
for (int i = 0; i < count; i++){
result = sqrt(pow(vertices[i].x, 2) + pow(vertices[i].y, 2) + pow(vertices[i].z, 2));
if (result > maxMagnitude)
maxMagnitude = result;
}
originalSphereRadius = maxMagnitude;
if (originalSphereRadius < 0)
originalSphereRadius *= -1;
boundingSphereRadius = maxMagnitude;
*/

/* METHOD 2:
D3DXVECTOR3 minDistance;
D3DXVECTOR3 maxDistance;
float maxMagnitude = 0;
float minMagnitude = 0;
float result = 0;
for (int i = 0; i < count; i++){
result = sqrt(pow(vertices[i].x, 2) + pow(vertices[i].y, 2) + pow(vertices[i].z, 2));
if (result > maxMagnitude){
maxMagnitude = result;
if (i == 0)
minMagnitude = maxMagnitude;
maxDistance.x = vertices[i].x;
maxDistance.y = vertices[i].y;
maxDistance.z = vertices[i].z;
}
if (result <= minMagnitude){
minMagnitude = result;
minDistance.x = vertices[i].x;
minDistance.y = vertices[i].y;
minDistance.z = vertices[i].z;
}
}

D3DXVECTOR3 sphereCentre;
D3DXVec3Add(&sphereCentre, &minDistance, &maxDistance);
sphereCentre *= 0.5f;
D3DXMatrixIdentity(boundingSphereCentre);
D3DXMatrixTranslation(boundingSphereCentre, sphereCentre.x, sphereCentre.x, sphereCentre.x);

originalSphereRadius = (sqrt(pow(maxDistance.x - minDistance.x, 2) + pow(maxDistance.y - minDistance.y, 2) + pow(maxDistance.z - minDistance.z, 2))) / 2;
boundingSphereRadius = originalSphereRadius;
*/

/* METHOD 3:
float maxMagnitude = 0;
float result = 0;
for (int i = 0; i < count; i++){
result = sqrt(pow(vertices[i].x, 2) + pow(vertices[i].y, 2) + pow(vertices[i].z, 2));
if (result > maxMagnitude)
maxMagnitude = result;
}
originalSphereRadius = maxMagnitude;
if (originalSphereRadius < 0)
originalSphereRadius *= -1;
boundingSphereRadius = maxMagnitude;
*/
#ifndef TIME_H
#define TIME_H

#ifdef G
#define GN __declspec(dllimport)
#else
#define GN __declspec(dllexport)
#endif

#include<Windows.h>

class GN Timer{
public:
	Timer();
	void firtsMeasure();
	void measure();
	float deltaTime();
	int fps();
private:
	int _fps;
	int fpsCounter;
	double _deltaTime;
	double measureSample;
	LARGE_INTEGER performanceCounter1;
	LARGE_INTEGER performanceCounter2;
	LARGE_INTEGER frequency;
};

#endif
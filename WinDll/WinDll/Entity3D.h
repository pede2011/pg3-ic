#ifndef ENTITY3D_H
#define ENTITY3D_H

#include"Entity2D.h"

class GN Entity3D : public Entity2D{
public:
	Entity3D();
	~Entity3D();
	virtual void translate(float x, float y, float z);
	virtual void setPosition(float x, float y, float z);
	virtual void rotate(float x, float y, float z);
	virtual void setRotation(float x, float y, float z);
	virtual void scale(float x, float y, float z);
	virtual void draw(DxClass * renderer) = 0;
	virtual void layer(int layer);
	float getZ() const;
	float getRotationX() const;
	float getRotationY() const;
	float getRotationZ() const;
	float getScaleZ() const;
protected:
	float posz, rotx, roty, rotz, scalez;
	int layerPos;
	void applyTransformation();
	void applyTransformation(D3DXMATRIX * parentMatrix);
	friend class GameObject;

	D3DXMATRIX* m_OriginalTransform;
};

#endif
#ifndef ANIMATION_H
#define ANIMATION_H

#include<Windows.h>
#include<vector>
#include"Timer.h"

struct GN Frame{
	double u1;
	double v1;
	double u2;
	double v2;
	double u3;
	double v3;
	double u4;
	double v4;
};

class GN Animation{
public:
	Animation();
	int currentFrame();
	void addFrame(float texWidth, float texHeight, float x, float y, float width, float height);
	void addFrame(Frame frame);
	std::vector<Frame> & frames();
	void setLenght(float lenght);
	void update(Timer * timer);
private:
	std::vector<Frame> _frames;
	int _currentFrame;
	float _lenght;
	float currentTime;
};

#endif
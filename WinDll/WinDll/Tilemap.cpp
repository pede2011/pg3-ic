#include"Tilemap.h"
#include<sstream>

void Tilemap::addTile(Sprite * tile, std::string canCollide){
	std::pair<Sprite *, std::string> tileInfo;
	tileInfo.first = tile;
	tileInfo.second = canCollide;
	tiles.insert(tileInfo);
}

void Tilemap::draw(DxClass * renderer){
	for (std::map<Sprite*, std::string>::iterator i = tiles.begin(); i != tiles.end(); i++){
		i->first->draw(renderer);
	}
}

void Tilemap::layer(int layer){
	for (std::map<Sprite*, std::string>::iterator i = tiles.begin(); i != tiles.end(); i++){
		i->first->layer(layer);
	}
}

void Tilemap::setActive(bool state){
	active = state;
	for (std::map<Sprite*, std::string>::iterator i = tiles.begin(); i != tiles.end(); i++){
		i->first->setActive(state);
	}
}

void Tilemap::translate(float x, float y){
	for (std::map<Sprite*, std::string>::iterator i = tiles.begin(); i != tiles.end(); i++){
		i->first->translate(x, y);
	}
}

void Tilemap::setPosition(float x, float y){
	for (std::map<Sprite*, std::string>::iterator i = tiles.begin(); i != tiles.end(); i++){
		i->first->setPosition(x,y);
	}
}

void Tilemap::rotate(float z){
	for (std::map<Sprite*, std::string>::iterator i = tiles.begin(); i != tiles.end(); i++){
		i->first->rotate(z);
	}
}

void Tilemap::scale(float x, float y){
	for (std::map<Sprite*, std::string>::iterator i = tiles.begin(); i != tiles.end(); i++){
		i->first->scale(x,y);
	}
}

void Tilemap::checkCollision(Entity2D * object){
	if (!active || !object->isActive())
		return;
	for (std::map<Sprite*, std::string>::iterator i = tiles.begin(); i != tiles.end(); i++){
		/*std::stringstream out;
		out << i->second;
		OutputDebugString(out.str().c_str());*/
		if (i->second == "true")
			collide(object, i->first);
	}
}

void Tilemap::collide(Entity2D * a, Entity2D * b){
	float overlapX = std::fmax(0.0f, std::fmin(a->getX() + fabs(a->getScaleX()) / 2.0f, b->getX() + fabs(b->getScaleX()) / 2.0f) - std::fmax(a->getX() - fabs(a->getScaleX()) / 2.0f, b->getX() - fabs(b->getScaleX()) / 2.0f));
	float overlapY = std::fmax(0.0f, std::fmin(a->getY() + fabs(a->getScaleY()) / 2.0f, b->getY() + fabs(b->getScaleY()) / 2.0f) - std::fmax(a->getY() - fabs(a->getScaleY()) / 2.0f, b->getY() - fabs(b->getScaleY()) / 2.0f));

	if (overlapX != 0 && overlapY != 0){
		if (overlapY >= overlapX){ // HORIZONTAL COLLISION
			if (a->getX() >= b->getX())
				a->translate(overlapX, 0);
			else
				a->translate(-overlapX, 0);
			return;
		}
		else{ // VERTICAL COLLISION
			if (a->getY() >= b->getY())
				a->translate(0, overlapY);
			else
				a->translate(0, -overlapY);
			return;
		}
	}
}

std::map<Sprite*, std::string> Tilemap::getTiles(){
	return tiles;
}

bool Tilemap::canBeDrawn(Camera * mainCamera, std::list<D3DXPLANE*> bspPlanes, bool drawPlanes){
	return true;
}
#include"TestGame.h"
#include"Importer.h"
#include"GameObject.h"

float speed = 0.02f;
int layer = 250;
Camera * cam;
GameObject * scene;
GameObject * player;

void TestGame::Start(){

	cam = new Camera();
	cam->SetPosition(0, 3, -15);
	setMainCamera(cam,90,1,1000);

	scene = new GameObject();
	scene->loadModelWithHierarchy("test.obj");
	add(scene);
	generateBspPlanes(scene);

	player = new GameObject();
	player->loadModelWithHierarchy("player.obj");
	player->translate(0, 1, -5);
	add(player);

	switchBspPlaneDraw();
}

void TestGame::Update(){
	if (input()->keyDown(Input::KEY_RIGHT)){
		cam->RotateRight(0.01f * timer()->deltaTime());
	}
	else if (input()->keyDown(Input::KEY_LEFT)){
		cam->RotateRight(-0.01f * timer()->deltaTime());
	}
	if (input()->keyDown(Input::KEY_UP)){
		cam->MoveForward(0.01f * timer()->deltaTime());
	}
	else if (input()->keyDown(Input::KEY_DOWN)){
		cam->MoveForward(-0.01f * timer()->deltaTime());
	}
	if (input()->keyDown(Input::KEY_Q)){
		cam->RotateDown(0.001f * timer()->deltaTime());
	}
	else if (input()->keyDown(Input::KEY_E)) {
		cam->RotateDown(-0.001f * timer()->deltaTime());
	}
	

	if (input()->keyDown(Input::KEY_A)){
		player->translate(0.01f * timer()->deltaTime(), 0, 0);
	}
	else if (input()->keyDown(Input::KEY_D)){
		player->translate(-0.01f * timer()->deltaTime(), 0, 0);
	}
	if (input()->keyDown(Input::KEY_S)){
		player->translate(0, 0, 0.01f * timer()->deltaTime());
	}
	else if (input()->keyDown(Input::KEY_W)){
		player->translate(0, 0, -0.01f * timer()->deltaTime());
	}
	
	if (input()->keyDown(Input::KEY_SPACE))
		switchBspPlaneDraw();
}

void TestGame::Shutdown(){
	delete scene;
	delete cam;
	delete player;
	cam = NULL;
	scene = NULL;
	player = NULL;
}
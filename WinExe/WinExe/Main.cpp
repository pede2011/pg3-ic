#include"Engine.h"
#include"TestGame.h"

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
	LPSTR lpCmdLine, int nCmdShow) {

	Engine eng(hInstance, 800, 600);

	Game * g = new TestGame();

	if (eng.init(g)){
		eng.RedirectIOToConsole();
		eng.run();
	}

	return 0;
}